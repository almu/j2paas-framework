## 项目案例

| 金融 | 教育/医疗 |物流/办公 | 房地产 |
|--- | --------|------|-----|
|国结产品线|	高校质量内涵数据采集与分析预警平台|	外贸管理系统|	房产自动撰写报告系统|
|外汇清算产品线|	大学资产管理系统|	物流管理系统（空运、汽运）|	房产智能评估系统|
|外汇资金产品线|	医院固定资产管理系统|	物流ERP管理系统|	房产评估OA管理系统|
|总分行报价平盘产品线|	医院物资管理系统|	外贸成本管理系统|	资产运营管理系统|
|银行国际业务系统|	医院供应室管理系统|	IT运维系统|	资产管理系统|
|银行资金交易系统|	智慧一卡通平台	|办公自动化系统	|租赁管理系统|
|金融交易系统|	|	人力资源管理系统|

## 项目截图

#### PC端

![case7.jpg](img/case/case7.png)

![case8.jpg](img/case/case8.png)

![case9.jpg](img/case/case9.png)

![case10.jpg](img/case/case10.png)

![case11.jpg](img/case/case11.png)

![case12.jpg](img/case/case12.png)

![case13.jpg](img/case/case13.png)

![case14.jpg](img/case/case14.png)

![case15.jpg](img/case/case15.png)

![case16.jpg](img/case/case16.png)

![case17.jpg](img/case/case17.png)

![case3.jpg](img/case/case3.png)
<br/>

![case18.jpg](img/case/case18.png)

![case19.jpg](img/case/case19.png)

![case6.jpg](img/case/case6.jpg)

#### 移动端

![admin.jpg](img/case/case20.jpg)
<br/>

![admin.jpg](img/case/case21.jpg)
<br/>

![admin.jpg](img/case/case22.jpg)
<br/>

![admin.jpg](img/case/case23.jpg)
<br/>

![admin.jpg](img/case/case24.jpg)
<br/>

![admin.jpg](img/case/case25.jpg)
<br/>

![admin.jpg](img/case/case26.jpg)
<br/>

![admin.jpg](img/case/case27.jpg)
<br/>

![admin.jpg](img/case/case28.jpg)
<br/>

![admin.jpg](img/case/case29.jpg)
<br/>