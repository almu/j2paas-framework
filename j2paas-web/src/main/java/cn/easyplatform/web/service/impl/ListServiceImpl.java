/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.service.impl;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.spi.engine.EngineFactory;
import cn.easyplatform.spi.service.ListService;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListServiceImpl implements ListService {

	private ListService broker = EngineFactory.me().getEngineService(
			ListService.class);

	@Override
	public IResponseMessage<?> doInit(ListInitRequestMessage req) {
		return broker.doInit(req);
	}

	@Override
	public IResponseMessage<?> doPaging(ListPagingRequestMessage req) {
		return broker.doPaging(req);
	}

	@Override
	public IResponseMessage<?> doQuery(ListQueryRequestMessage req) {
		return broker.doQuery(req);
	}

	@Override
	public IResponseMessage<?> doRefresh(ListRefreshRequestMessage req) {
		return broker.doRefresh(req);
	}

	@Override
	public IResponseMessage<?> doSave(ListSaveRequestMessage req) {
		return broker.doSave(req);
	}

	@Override
	public IResponseMessage<?> doDelete(ListDeleteRequestMessage req) {
		return broker.doDelete(req);
	}

	@Override
	public IResponseMessage<?> doCreate(ListCreateRequestMessage req) {
		return broker.doCreate(req);
	}

	@Override
	public IResponseMessage<?> doSelect(ListSelectRequestMessage req) {
		return broker.doSelect(req);
	}

	@Override
	public IResponseMessage<?> doClose(SimpleRequestMessage req) {
		return broker.doClose(req);
	}

	@Override
	public IResponseMessage<?> doOpen(ListOpenRequestMessage req) {
		return broker.doOpen(req);
	}

	@Override
	public IResponseMessage<?> doReload(ListReloadRequestMessage req) {
		return broker.doReload(req);
	}

	@Override
	public IResponseMessage<?> doSort(ListSortRequestMessage req) {
		return broker.doSort(req);
	}

	@Override
	public IResponseMessage<?> doSelection(ListSelectionRequestMessage req) {
		return broker.doSelection(req);
	}
	
	public IResponseMessage<?> doClear(SimpleTextRequestMessage req){
		return broker.doClear(req);
	}

	@Override
	public IResponseMessage<?> doReset(ListResetRequestMessage req) {
		return broker.doReset(req);
	}

	@Override
	public IResponseMessage<?> doDrop(DropRequestMessage req) {
		return broker.doDrop(req);
	}

	@Override
	public IResponseMessage<?> evalList(ListEvalRequestMessage req) {
		return broker.evalList(req);
	}

	@Override
	public IResponseMessage<?> setPageSize(ListPagingRequestMessage req) {
		return broker.setPageSize(req);
	}

	@Override
	public IResponseMessage<?> doLoad(SimpleRequestMessage req) {
		return broker.doLoad(req);
	}
}
