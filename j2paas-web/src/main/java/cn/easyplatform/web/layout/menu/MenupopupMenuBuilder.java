/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.menu;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.*;
import cn.easyplatform.web.layout.IMenuBuilder;
import cn.easyplatform.web.listener.RunTaskListener;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;


/**
 * @author create by <a href="mailto:davidchen@epclouds.com">littleDog</a> on
 *         2015-6-26下午3:24:57<br/>
 *         <br/>
 * @since 2.0.0 <br/>
 */
public class MenupopupMenuBuilder implements IMenuBuilder {

    private Menupopup mp;

    private Component container;

    public MenupopupMenuBuilder(Menupopup mp) {
        this.mp = mp;
    }

    @Override
    public void fill(AuthorizationVo av) {
        if (av == null || av.getRoles() == null)
            return;
        this.container = mp.getFellow("gv5container");
        Component ref = mp.getFirstChild();
        if (av.getRoles().size() == 1 && av.getAgents() != null
                && av.getAgents().isEmpty()) {
            RoleVo rv = av.getRoles().get(0);
            for (MenuVo mv : rv.getMenus())
                createMenu(rv, null, mp, mv, ref);
        } else {
            if (av.getRoles().size() == 1) {
                RoleVo rv = av.getRoles().get(0);
                for (MenuVo mv : rv.getMenus())
                    createMenu(rv, null, mp, mv, null);
            } else {
                for (RoleVo rv : av.getRoles()) {
                    Menu menu = new Menu(rv.getName());
                    if (Strings.isBlank(rv.getImage()))
                        menu.setImage("/images/group.png");
                    else
                        menu.setImage("/images/" + rv.getImage());
                    Menupopup pop = new Menupopup();
                    for (MenuVo mv : rv.getMenus())
                        createMenu(rv, null, pop, mv, null);
                    pop.setParent(menu);
                    mp.insertBefore(menu, ref);
                }
                if (av.getAgents() != null && !av.getAgents().isEmpty()) {
                    for (AgentVo agent : av.getAgents()) {
                        for (RoleVo rv : agent.getRoles()) {
                            Menu menu = new Menu(rv.getName());
                            if (Strings.isBlank(rv.getImage()))
                                menu.setImage("/images/group.png");
                            else
                                menu.setImage("/images/" + rv.getImage());
                            Menupopup pop = new Menupopup();
                            for (MenuVo mv : rv.getMenus())
                                createMenu(rv, agent, pop, mv, null);
                            pop.setParent(menu);
                            mp.insertBefore(menu, ref);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param rv
     * @param agent
     * @param pop
     * @param mv
     */
    private void createMenu(RoleVo rv, AgentVo agent, Menupopup parent,
                            MenuVo mv, Component ref) {
        Menu menu = new Menu(mv.getName());
        menu.setTooltiptext(mv.getName());
        if (Strings.isBlank(mv.getImage()))
            menu.setIconSclass("z-icon-hand-o-right z-menu-image");
        else
            menu.setImage("/images/" + mv.getImage());
        menu.setParent(parent);
        if (mv.getChildMenus() != null && !mv.getChildMenus().isEmpty()) {
            Menupopup pop = new Menupopup();
            for (MenuVo vo : mv.getChildMenus())
                createMenu(rv, agent, pop, vo, null);
            pop.setParent(menu);
            if (ref == null)
                menu.setParent(parent);
            else
                parent.insertBefore(menu, ref);
        } else if (mv.getTasks() != null && !mv.getTasks().isEmpty()) {
            Menupopup pop = new Menupopup();
            pop.setParent(menu);
            for (MenuNodeVo tv : mv.getTasks()) {
                Menuitem item = new Menuitem(tv.getName());
                tv.setRoleId(rv.getId());
                if (agent != null)
                    tv.setAgent(agent.getId());
                item.setParent(pop);
                PageUtils.setTaskIcon(item, tv.getImage());
                item.setTooltiptext(tv.getDesp() == null ? tv.getName() : tv
                        .getDesp());
                item.addEventListener(Events.ON_CLICK, new RunTaskListener(tv, this.container));
            }
        }
    }

}
