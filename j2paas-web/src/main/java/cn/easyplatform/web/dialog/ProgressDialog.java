/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.dialog;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.executor.BeginVo;
import cn.easyplatform.messages.vos.executor.EndVo;
import cn.easyplatform.messages.vos.executor.ErrorVo;
import cn.easyplatform.messages.vos.executor.ProgressVo;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Window;

public class ProgressDialog {

    private Window main;

    private Label row;

    private Label total;

    private Label starttime;

    private Label spendtime;

    private long beginTime;

    private Progressmeter progressmeter;

    public ProgressDialog(Page page) {
        main = (Window) Executions.createComponents("~./pages/message.zul", null, null);
        Clients.showBusy(Labels.getLabel("service.long.operation"));
        row = (Label) main.query("#row");
        total = (Label) main.query("#total");
        starttime = (Label) main.query("#starttime");
        spendtime = (Label) main.query("#spendtime");
        progressmeter = (Progressmeter) main.query("#progressmeter");
        row.setValue("1");
    }

    public void display(Object msg) {
        if (msg instanceof BeginVo) {
            BeginVo bv = (BeginVo) msg;
            main.setTitle(bv.getTitle());
            if (bv.getTotal() == -1) {
                total.setVisible(false);
                row.setVisible(false);
            } else {
                total.setVisible(true);
                row.setVisible(true);
                total.setValue(String.valueOf(bv.getTotal()));
                row.setValue("0");
            }
            if (Strings.isBlank(starttime.getValue())) {
                beginTime = System.currentTimeMillis();
                starttime.setValue(DateFormatUtils.format(beginTime, "HH:mm:ss"));
            }
            Clients.clearBusy();
        } else if (msg instanceof ProgressVo) {
            ProgressVo pv = (ProgressVo) msg;
            row.setValue(String.valueOf(pv.getNo()));
            float pos = pv.getNo() / Float.parseFloat(total.getValue());
            progressmeter.setValue(Math.round(pos * 100));
            long sec = (System.currentTimeMillis() - beginTime) / 1000;
            spendtime.setValue(Long.toString(sec));
        } else if (msg instanceof ErrorVo) {
            main.setClosable(true);
            main.getChildren().clear();
            Label label = new Label(((ErrorVo) msg).getMsg());
            label.setZclass("m-1 text-danger");
            main.appendChild(label);
            Clients.clearBusy();
        } else {
            EndVo endVo = (EndVo) msg;
            if (endVo.getResult() instanceof Object[]) {
                Object[] result = (Object[]) endVo.getResult();
                int val = ((Number) result[0]).intValue();
                float pos = val / Float.parseFloat(total.getValue());
                progressmeter.setValue(Math.round(pos * 100));
                main.setClosable(true);
                row.setValue(String.valueOf(val));
                main.setTitle(main.getTitle() + ":" + Labels.getLabel("message.long.result", result));
            } else {
                main.detach();
                main = null;
            }
            Clients.clearBusy();
        }
    }
}
