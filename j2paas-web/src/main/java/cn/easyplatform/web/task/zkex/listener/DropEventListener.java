/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.listener;

import cn.easyplatform.messages.vos.datalist.ListDropVo;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.ext.Node;
import cn.easyplatform.web.ext.zul.Loader;
import cn.easyplatform.web.task.zkex.DropSupport;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Treeitem;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DropEventListener implements EventListener<DropEvent> {

    private DropSupport os;

    public DropEventListener(DropSupport os) {
        this.os = os;
    }

    @Override
    public void onEvent(DropEvent evt) throws Exception {
        Component c = evt.getDragged();
        List<ListRowVo> data = new ArrayList<ListRowVo>();
        String srcId = null;
        Loader ref = null;
        boolean fromList = false;
        if (c instanceof Listitem) {
            Listitem li = (Listitem) c;
            fromList = li.getValue() instanceof ListRowVo;
            if (li.getListbox().getSelectedCount() > 1) {
                for (Listitem item : li.getListbox().getSelectedItems()) {
                    if (fromList)
                        data.add((ListRowVo) item.getValue());
                    else if (item.getValue() instanceof FieldVo[])
                        data.add(new ListRowVo((FieldVo[]) item.getValue()));
                    else {
                        Node node = item.getValue();
                        if (node.getData() != null)
                            data.add(new ListRowVo(node.getData()));
                    }
                }
            } else if (fromList)
                data.add((ListRowVo) li.getValue());
            else if (li.getValue() instanceof FieldVo[])
                data.add(new ListRowVo((FieldVo[]) li.getValue()));
            else {
                Node node = li.getValue();
                if (node.getData() != null)
                    data.add(new ListRowVo(node.getData()));
            }
            if (fromList)
                srcId = li.getListbox().getId();
            ref = (Loader) li.getListbox().getAttribute("ref");
        } else {
            Treeitem ti = (Treeitem) c;
            fromList = ti.getValue() instanceof ListRowVo;
            if (ti.getTree().getSelectedCount() > 1) {
                for (Treeitem item : ti.getTree().getSelectedItems()) {
                    if (fromList)
                        data.add((ListRowVo) item.getValue());
                    else if (item.getValue() instanceof FieldVo[])
                        data.add(new ListRowVo((FieldVo[]) item.getValue()));
                    else {
                        Node node = item.getValue();
                        if (node.getData() != null)
                            data.add(new ListRowVo(node.getData()));
                    }
                }
            } else if (fromList)
                data.add((ListRowVo) ti.getValue());
            else if (ti.getValue() instanceof FieldVo[])
                data.add(new ListRowVo((FieldVo[]) ti.getValue()));
            else {
                Node node = ti.getValue();
                if (node.getData() != null)
                    data.add(new ListRowVo(node.getData()));
            }
            if (fromList)
                srcId = ti.getTree().getId();
            ref = (Loader) ti.getTree().getAttribute("ref");
        }
        if (!data.isEmpty()) {
            Events.postEvent(new Event(Events.ON_USER, ref, "onBefore"));
            os.drop(new ListDropVo(fromList ? ref.getFilter() : ref.getMapping(), srcId, data));
            Events.postEvent(new Event(Events.ON_USER, ref, "onAfter"));
        }
    }

}
