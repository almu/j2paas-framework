/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.message.entity;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.spi.listener.event.Event;
import org.apache.commons.lang3.ArrayUtils;
import org.zkoss.lang.Objects;

/**
 * 系统内置应用侦听者
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Message {

    public final static String CONSOLE = "EPConsole";

    public final static String LOG = "EPLog";

    private String projectId;

    private Event entity;

    private String[] target;

    public boolean contain(String projectId, String userId) {
        if (Strings.isBlank(projectId))
            return true;
        if (Objects.equals(projectId, this.projectId)) {
            if (target == null || target.length == 0)
                return true;
            return ArrayUtils.contains(target, userId);
        }
        return false;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void setTarget(String[] target) {
        this.target = target;
    }

    public Event getEntity() {
        return entity;
    }

    public void setEntity(Event entity) {
        this.entity = entity;
    }
}
