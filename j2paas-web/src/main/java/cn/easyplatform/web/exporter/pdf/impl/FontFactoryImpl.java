/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.exporter.pdf.impl;

import cn.easyplatform.web.exporter.pdf.FontFactory;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.BaseFont;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class FontFactoryImpl implements FontFactory {

	private static BaseFont bfChinese = null;

	static {
		try {
			bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",
					BaseFont.NOT_EMBEDDED);
		} catch (Exception ex) {
		}
	}

	protected float defaultFontSize = 10;
	protected BaseColor defaultFontColor = WebColors.getRGBColor("#636363");

	protected Font getHeaderFont() {
		Font font = getDefaultFont();
		font.setStyle(Font.BOLD);
		return font;
	}

	protected Font getCellFont() {
		return getDefaultFont();
	}

	protected Font getGroupFont() {
		Font font = getDefaultFont();
		font.setStyle(Font.BOLD);
		return font;
	}

	protected Font getGroupfootFont() {
		Font font = getDefaultFont();
		font.setStyle(Font.BOLD);
		return font;
	}

	protected Font getFooterFont() {
		return getDefaultFont();
	}

	protected Font getDefaultFont() {
		Font font = new Font(bfChinese);
		font.setColor(defaultFontColor);
		font.setSize(defaultFontSize);
		return font;
	}

	public Font getFont(String type) {
		if (FontFactory.FONT_TYPE_HEADER.equals(type)) {
			return getHeaderFont();
		} else if (FontFactory.FONT_TYPE_CELL.equals(type)) {
			return getCellFont();
		} else if (FontFactory.FONT_TYPE_GROUP.equals(type)) {
			return getGroupFont();
		} else if (FontFactory.FONT_TYPE_GROUPFOOT.equals(type)) {
			return getGroupfootFont();
		} else if (FontFactory.FONT_TYPE_FOOTER.equals(type)) {
			return getFooterFont();
		}
		return getDefaultFont();
	}
}
