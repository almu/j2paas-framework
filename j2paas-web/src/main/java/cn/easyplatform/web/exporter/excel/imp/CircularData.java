/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.exporter.excel.imp;

import java.util.Locale;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CircularData {
	public static final int UPPER = 2;
	public static final int LOWER = 1;
	public static final int NORMAL = 0;
	
	private final String[] _data;
	private final Locale _locale;

	/*package*/ CircularData(String[] dataKey, int type, Locale locale) { //ZSS-69
		_locale = locale;
		_data = type == LOWER ? getDataL(dataKey) : type == UPPER ? getDataU(dataKey) : getDataN(dataKey);
	}
	/*package*/ String getData(int current) {
		return _data[current];
	}
	/*package*/ int getIndex(String x) {
		for(int j = 0; j < _data.length; ++j) {
			if (_data[j].equalsIgnoreCase(x)) {
				return j;
			}
		}
		return -1;
	}
	/*package*/ int getIndexByStartsWith(String x) { //ZSS-67
		for(int j = 0; j < _data.length; ++j) {
			if (_data[j].startsWith(x))
				return j;
		}
		return -1;
	}
	/*package*/ int getSize() {
		return _data.length;
	}
	private String[] getDataN(String[] dataKey) { //normal. E.g. Sunday, Monday...
		//i18n
		return dataKey;
	}
	private String[] getDataL(String[] dataKey) { //all lowercase. E.g. sunday, monday...
		final String[] weekfn = getDataN(dataKey);
		final String[] weekL = new String[weekfn.length];
		for(int j = 0; j < weekfn.length; ++j) {
			weekL[j] = weekfn[j].toLowerCase(_locale); //ZSS-69
		}
		return weekL;
	}
	private String[] getDataU(String[] dataKey) { //all uppercase. E.g. SUNDAY, MONDAY...
		final String[] weekfn = getDataN(dataKey);
		final String[] weekU = new String[weekfn.length];
		for(int j = 0; j < weekfn.length; ++j) {
			weekU[j] = weekfn[j].toUpperCase(_locale); //ZSS-69
		}
		return weekU;
	}
}
