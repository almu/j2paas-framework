/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.ocez;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class OrgNode implements Serializable, Cloneable {

    private String id;

    private String name;

    private String title;

    private String img;

    private String className;

    private String relationship;

    private List<OrgNode> children;

    private transient int level = 0;

    private transient Object parentKey;

    private transient OrgNode parent;

    public OrgNode(String id, String name) {
        this(id, name, null);
    }

    public OrgNode(String id, String name, String title) {
        this.id = id;
        this.name = name;
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public Object getParentKey() {
        return parentKey;
    }

    public void setParentKey(Object parentKey) {
        this.parentKey = parentKey;
    }

    public List<OrgNode> getChildren() {
        return children;
    }

    public void appendChild(OrgNode node) {
        if (children == null)
            children = new ArrayList<>();
        node.level = this.level + 1;
        node.className = "level-" + node.level;
        node.parent = this;
        children.add(node);
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public OrgNode getParent() {
        return parent;
    }

    public boolean isLeaf() {
        return children == null || children.isEmpty();
    }

    public OrgNode clone(boolean deep) {
        try {
            OrgNode clone = (OrgNode) super.clone();
            if (deep && children != null) {
                clone.children = new ArrayList<>(children.size());
                for (OrgNode child : children) {
                    OrgNode node = new OrgNode(child.getId(), child.getName(), child.getTitle());
                    node.img = child.img;
                    node.level = child.level;
                    node.parent = clone;
                    node.className = child.className;
                    node.relationship = child.relationship;
                    clone.children.add(node);
                }
            } else
                clone.children = null;
            return clone;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
