<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>

#canvas {
    height: 100%;
    width: 100%;
    padding: 0;
    margin: 0;
}

.diagram-note {
    background-color: rgba(66, 180, 21, 0.7);
    color: White;
    border-radius: 5px;
    font-family: Arial;
    font-size: 12px;
    padding: 5px;
    min-height: 16px;
    width: 50px;
    text-align: center;
}

.needs-discussion:not(.djs-connection) .djs-visual > :nth-child(1) {
    stroke: rgba(66, 180, 21, 0.7) !important;
}

.j2pass-bpmn-Panel {
    background-color:#f8f8f8;
    width:280px;
    height:100%;
    position:absolute;
    top:0%;
    right:0;
    border-left:solid 1px #cccccc;
    box-shadow: 0 0 2px rgb(0,0,0,0.3);
    font-family: Arial, sans-serif;
}

.j2pass-bpmn-Panel-toggle-div {
    position: absolute;
    left: -30px;
    top: 50%;
    background-color:#f8f8f8;
    padding: 7px 10px;
    transform: rotate(-90deg);
    white-space: nowrap;
    font-size: 13px;
    border: solid 1px #cccccc;
    border-bottom: none;
    border-radius: 2px 2px 0 0;
    transform-origin: top left;
    z-index: 10;
    cursor: default;
    user-select: none;
}

.j2pass-bpmn-Panel-north {
    padding: 18px;

}

.j2pass-bpmn-Panel-center {
    padding: 0px 18px 0px 18px;
    border-bottom: 1px solid #cccccc;
    box-sizing: border-box;
    user-select: none;
}

.j2pass-bpmn-Panel-south {
    padding: 18px;

}

.j2pass-bpmn-button {

    border-style: solid;
    border-width: 1px;
    border-color: #088ef0;
    color: #FFF;
    background: linear-gradient(#34a5f8, #088ef0);
    box-shadow: inset 0px 1px 0px rgb(255 255 255 / 30%), 0 1px 2px rgb(0 0 0 / 15%);
    font-size: 12px;
    border-radius: 200px;
    font-family: "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    text-decoration: none;
    text-align: center;
    height: 40px;
    appearance: none;
    cursor: pointer;
    border: none;
    box-sizing: border-box;
    transition-duration: .3s;
    width:100px;
    height:30px;
    position:absolute;
    top:85%;
    right:25%;
}

.j2pass-bpmn-input {
    display: block;
    font-size: 14px;
    padding: 3px 6px;
    border: 1px solid #cccccc;
    vertical-align: middle;
    margin-top: 2px;
    width: 92%;
}

.j2pass-bpmn-title1-label {
    font-size:20px;
    font-weight: bolder;
}

.j2pass-bpmn-title2-label {
    font-weight: bolder;
    display: inline-block;
    vertical-align: middle;
    color: #444444;
    margin-bottom: 3px;
    font-size: 18px;
    display: block;
}

.j2pass-bpmn-title3-label {
    font-weight: bolder;
    display: inline-block;
    vertical-align: middle;
    color: #666666;
    margin-bottom: 3px;
    font-size: 16px;

}

.j2pass-bpmn-title4-label {
    font-weight: bolder;
    display: inline-block;
    vertical-align: middle;
    color: #666666;
    font-size: 12px;
}

.j2pass-bpmn-right-arrow {
    float: right;
    margin-right: -18px;
    font-size: 16px;
    padding: 3px 4px 3px 4px;
    color: #666666;
}

.j2pass-bpmn-left-arrow {
    float: left;
    margin-left: -18px;
    font-size: 16px;
    padding: 3px 4px 3px 4px;
    color: #666666;
}

.j2pass-bpmn-ul {
    margin: 5px 0 -1px 0;
    padding: 0;
    white-space: nowrap;
    overflow: hidden;
    display: block;
    list-style-type: disc;
}

.j2pass-bpmn-li {
    display: inline-block;
    margin: 0px 10px 0px 3px;
}

.j2pass-bpmn-a-select {
    top: 2px;
    position: relative;
    padding-bottom: 5px;
    border-top: 3px solid #4d90ff;
    border-bottom: none !important;
    display: inline-block;
    font-size: 14px;
    padding: 5px 12px;
    border-left: 1px solid #cccccc;
    border-right: 1px solid #cccccc;
    border-radius: 3px 3px 0 0;
    background-color: #f8f8f8;
    color: #666666;
    text-decoration: none;
    cursor: default;
}

.j2pass-bpmn-a-unselect {
    position: relative;
    padding-bottom: 5px;
    display: inline-block;
    font-size: 14px;
    padding: 5px 12px;
    border: 1px solid #cccccc;
    border-radius: 3px 3px 0 0;
    background-color: #f8f8f8;
    color: #666666;
    text-decoration: none;
    cursor: default;
}

.j2pass-bpmn-Panel-center-div {
    border-top: 1px solid #eeeeee;
    position: relative;
    margin-top: 15px;
}

.j2pass-bpmn-Panel-center-check-div {
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
    align-items: center;
    margin-top: 5px;
}

.j2pass-bpmn-checkbox {
    font-size: 10px !important;
    border: 1px solid #cccccc;
    vertical-align: middle;
    margin: 0px 6px 0px 0px !important;
    width: 15px !important;
    height: 15px !important;
}

.j2pass-bpmn-panel-property-button {
    position: relative;
    margin-left: 10px;
    top: 0;
    height: 22px;
    width: 22px;
    overflow: hidden;
    background-color: #f8f8f8;
    border: 1px solid #cccccc;
    font-size: 14px;
    padding: 1px 5px;
}

.j2pass-bpmn-panel-property-span:hover{
    color: #005df7;
}

.j2pass-bpmn-panel-property-span{
    font-weight: bolder;
    color: #666666;
}

.j2pass-bpmn-panel-add-property-div{
    margin-bottom: 2px;
    overflow: hidden;
}

.j2pass-bpmn-panel-add-property-label{
    width: 42%;
    box-sizing: border-box;
    padding-left: 5px;
    font-weight: bolder;
    display: inline-block;
    vertical-align: middle;
    color: #666666;
    margin-bottom: 3px;
    font-family: Arial, sans-serif;
    font-size: 12px;
}

.j2pass-bpmn-panel-add-property-input{
    width: calc(40% - 20px);
    font-size: 12px;
    padding: 3px 6px;
    border: 1px solid #cccccc;
    float: left;
    vertical-align: middle;
}