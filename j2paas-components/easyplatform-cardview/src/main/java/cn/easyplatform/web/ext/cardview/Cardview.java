/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.cardview;

import cn.easyplatform.web.annotation.Builder;
import cn.easyplatform.web.ext.Reloadable;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.ZkExt;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.zkoss.zk.au.AuRequest;
import cn.easyplatform.lang.Strings;


import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.impl.XulElement;

@Builder(CardviewBuilder.class)
public class Cardview extends XulElement implements ZkExt, Reloadable {

    public final static String ON_Touch = "onTouch";
    public final static String ON_FrontLink = "onFrontLink";
    public final static String ON_CentreLink = "onCentreLink";
    public final static String ON_TailLink = "onTailLink";

    static {
        addClientEvent(Cardview.class, ON_FrontLink, CE_IMPORTANT);
        addClientEvent(Cardview.class, ON_CentreLink, CE_IMPORTANT);
        addClientEvent(Cardview.class, ON_TailLink, CE_IMPORTANT);
        addClientEvent(Cardview.class, ON_Touch, CE_IMPORTANT);
    }

    private String[] img;
    private String[] title;
    private String[] author;
    private String[] vardate;
    private String[] portrait;
    private String[] contenturl;
    private String[] contentevt;
    private String[] fronturl;
    private String[] frontevt;
    private String[] centreurl;
    private String[] centreevt;
    private String[] tailurl;
    private String[] tailevt;
    private boolean portraitflag;
    private String[] header;
    private String[] flagcolour;
    private String[] flagcontent;
    private String iconstyle;
    private String fronttext;
    private String centretext;
    private String tailtext;
    private String linkstyle;
    private String[] sequence;
    private String[] contenta;
    private String[] contentb;
    private String[] contentc;
    private String[] contentd;
    private String[] contente;
    private String[] msgDate;
    private String[] msgStatus;
    private String extraEvent;
    private boolean force;
    private boolean reloadFlag;

    private String query;
    private String dbId;
    private String filter;

    public String[] getImg() {
        return img;
    }

    public void setImg(String img) {
        if (!Strings.isBlank(img)) {
            try {
                JSONArray js = JSON.parseArray(img);
                this.img = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("img", this.img);
        }
    }

    public String[] getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (!Strings.isBlank(title)) {
            try {
                JSONArray js = JSON.parseArray(title);
                this.title = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("title", this.title);
        }
    }

    public String[] getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        if (!Strings.isBlank(author)) {
            try {
                JSONArray js = JSON.parseArray(author);
                this.author = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("author", this.author);
        }
    }

    public String[] getVardate() {
        return vardate;
    }

    public void setVardate(String vardate) {
        if (!Strings.isBlank(vardate)) {
            try {
                JSONArray js = JSON.parseArray(vardate);
                this.vardate = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("vardate", this.vardate);
        }
    }

    public String[] getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        if (!Strings.isBlank(portrait)) {
            try {
                JSONArray js = JSON.parseArray(portrait);
                this.portrait = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("portrait", this.portrait);
        }
    }

    public String[] getFronturl() {
        return fronturl;
    }

    public void setFronturl(String fronturl) {
        if (!Strings.isBlank(fronturl)) {
            try {
                JSONArray js = JSON.parseArray(fronturl);
                this.fronturl = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("fronturl", this.fronturl);
        }
    }

    public String[] getFrontevt() {
        return frontevt;
    }

    public void setFrontevt(String frontevt) {
        if (!Strings.isBlank(frontevt)) {
            try {
                JSONArray js = JSON.parseArray(frontevt);
                this.frontevt = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("frontevt", this.frontevt);
        }
    }

    public String[] getCentreurl() {
        return centreurl;
    }

    public void setCentreurl(String centreurl) {
        if (!Strings.isBlank(centreurl)) {
            try {
                JSONArray js = JSON.parseArray(centreurl);
                this.centreurl = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("centreurl", this.centreurl);
        }
    }

    public String[] getCentreevt() {
        return centreevt;
    }

    public void setCentreevt(String centreevt) {
        if (!Strings.isBlank(centreevt)) {
            try {
                JSONArray js = JSON.parseArray(centreevt);
                this.centreevt = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("centreevt", this.centreevt);
        }
    }

    public String[] getTailurl() {
        return tailurl;
    }

    public void setTailurl(String tailurl) {
        if (!Strings.isBlank(tailurl)) {
            try {
                JSONArray js = JSON.parseArray(tailurl);
                this.tailurl = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("tailurl", this.tailurl);
        }
    }

    public String[] getTailevt() {
        return tailevt;
    }

    public void setTailevt(String tailevt) {
        if (!Strings.isBlank(tailevt)) {
            try {
                JSONArray js = JSON.parseArray(tailevt);
                this.tailevt = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("tailevt", this.tailevt);
        }
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        if (!Strings.isBlank(query)) {
            this.query = query;
            smartUpdate("query", this.query);
        }
    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String dbId) {
        if (!Strings.isBlank(dbId)) {
            this.dbId = dbId;
            smartUpdate("dbId", this.dbId);
        }
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        if (!Strings.isBlank(filter)) {
            this.filter = filter;
            smartUpdate("filter", this.filter);
        }
    }

    public boolean getPortraitflag() {
        return portraitflag;
    }

    public void isPortraitflag(boolean portraitflag) {
        this.portraitflag = portraitflag;
    }

    public String[] getHeader() {
        return header;
    }

    public void setHeader(String header) {
        if (!Strings.isBlank(header)) {
            try {
                JSONArray js = JSON.parseArray(header);
                this.header = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("header", this.header);
        }
    }

    public String[] getFlagcolour() {
        return flagcolour;
    }

    public void setFlagcolour(String flagcolour) {
        if (!Strings.isBlank(flagcolour)) {

            try {
                JSONArray js = JSON.parseArray(flagcolour);
                this.flagcolour = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("flagcolour", this.flagcolour);
        }
    }

    public String[] getFlagcontent() {
        return flagcontent;
    }

    public void setFlagcontent(String flagcontent) {
        if (!Strings.isBlank(flagcontent)) {
            try {
                JSONArray js = JSON.parseArray(flagcontent);
                this.flagcontent = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("flagcontent", this.flagcontent);
        }
    }

    public String getIconstyle() {
        return iconstyle;
    }

    public void setIconstyle(String iconstyle) {
        if (!Strings.isBlank(iconstyle)) {
            this.iconstyle = iconstyle;
            smartUpdate("iconstyle", this.iconstyle);
        }
    }

    public String getFronttext() {
        return fronttext;
    }

    public void setFronttext(String fronttext) {
        if (!Strings.isBlank(fronttext)) {
            this.fronttext = fronttext;
            smartUpdate("fronttext", this.fronttext);
        }
    }

    public String getCentretext() {
        return centretext;
    }

    public void setCentretext(String centretext) {
        if (!Strings.isBlank(centretext)) {
            this.centretext = centretext;
            smartUpdate("centretext", this.centretext);
        }
    }

    public String getTailtext() {
        return tailtext;
    }

    public void setTailtext(String tailtext) {
        if (!Strings.isBlank(tailtext)) {
            this.tailtext = tailtext;
            smartUpdate("tailtext", this.tailtext);
        }
    }

    public String getLinkstyle() {
        return linkstyle;
    }

    public void setLinkstyle(String linkstyle) {
        if (!Strings.isBlank(linkstyle)) {
            this.linkstyle = linkstyle;
            smartUpdate("linkstyle", this.linkstyle);
        }
    }

    public String[] getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        if (!Strings.isBlank(sequence)) {
            try {
                JSONArray js = JSON.parseArray(sequence);
                this.sequence = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("sequence", this.sequence);
        }
    }

    public String[] getContenta() {
        return contenta;
    }

    public void setContenta(String contenta) {
        if (!Strings.isBlank(contenta)) {
            try {
                JSONArray js = JSON.parseArray(contenta);
                this.contenta = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("contenta", this.contenta);
        }
    }

    public String[] getContentb() {
        return contentb;
    }

    public void setContentb(String contentb) {
        if (!Strings.isBlank(contentb)) {
            try {
                JSONArray js = JSON.parseArray(contentb);
                this.contentb = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("contentb", this.contentb);
        }
    }

    public String[] getContentc() {
        return contentc;
    }

    public void setContentc(String contentc) {
        if (!Strings.isBlank(contentc)) {
            try {
                JSONArray js = JSON.parseArray(contentc);
                this.contentc = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("contentc", this.contentc);
        }
    }

    public String[] getContentd() {
        return contentd;
    }

    public void setContentd(String contentd) {
        if (!Strings.isBlank(contentd)) {
            try {
                JSONArray js = JSON.parseArray(contentd);
                this.contentd = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("contentd", this.contentd);
        }
    }

    public String[] getContente() {
        return contente;
    }

    public void setContente(String contente) {
        if (!Strings.isBlank(contente)) {
            try {
                JSONArray js = JSON.parseArray(contente);
                this.contente = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("contente", this.contente);
        }
    }

    public String[] getContenturl() {
        return contenturl;
    }

    public void setContenturl(String contenturl) {
        if (!Strings.isBlank(contenturl)) {
            try {
                JSONArray js = JSON.parseArray(contenturl);
                this.contenturl = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("contenturl", this.contenturl);
        }
    }

    public String[] getContentevt() {
        return contentevt;
    }

    public void setContentevt(String contentevt) {
        if (!Strings.isBlank(contentevt)) {
            try {
                JSONArray js = JSON.parseArray(contentevt);
                this.contentevt = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("contentevt", this.contentevt);
        }
    }

    public String[] getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(String msgDate) {
        if (!Strings.isBlank(msgDate)) {
            try {
                JSONArray js = JSON.parseArray(msgDate);
                this.msgDate = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("msgDate", this.msgDate);
        }
    }

    public String getExtraEvent() {
        return extraEvent;
    }

    public void setExtraEvent(String extraEvent) {
        if (!Strings.isBlank(extraEvent)) {
            this.extraEvent = extraEvent;
            smartUpdate("extraEvent", this.extraEvent);
        }
    }

    public boolean isReloadFlag() {
        return reloadFlag;
    }

    public void setReloadFlag(boolean reloadFlag) {
        this.reloadFlag = reloadFlag;
        smartUpdate("reloadFlag", this.reloadFlag);
    }

    public String[] getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(String msgStatus) {
        try {
            JSONArray js = JSON.parseArray(msgStatus);
            this.msgStatus = js.toArray(new String[]{});
        } catch (Exception e) {
            e.printStackTrace();
        }
        smartUpdate("msgStatus", this.msgStatus);
    }

    @Override
    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    //super//
    protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)
            throws java.io.IOException {
        super.renderProperties(renderer);

        render(renderer, "img", img);
        render(renderer, "title", title);
        render(renderer, "author", author);
        render(renderer, "vardate", vardate);
        render(renderer, "portrait", portrait);
        render(renderer, "contenturl", contenturl);
        render(renderer, "contentevt", contentevt);
        render(renderer, "fronturl", fronturl);
        render(renderer, "frontevt", frontevt);
        render(renderer, "centreurl", centreurl);
        render(renderer, "centreevt", centreevt);
        render(renderer, "tailurl", tailurl);
        render(renderer, "tailevt", tailevt);
        render(renderer, "query", query);
        render(renderer, "dbId", dbId);
        render(renderer, "filter", filter);
        render(renderer, "portraitflag", portraitflag);
        render(renderer, "header", header);
        render(renderer, "flagcolour", flagcolour);
        render(renderer, "flagcontent", flagcontent);
        render(renderer, "iconstyle", iconstyle);
        render(renderer, "fronttext", fronttext);
        render(renderer, "centretext", centretext);
        render(renderer, "tailtext", tailtext);
        render(renderer, "linkstyle", linkstyle);
        render(renderer, "sequence", sequence);
        render(renderer, "contenta", contenta);
        render(renderer, "contentb", contentb);
        render(renderer, "contentc", contentc);
        render(renderer, "contentd", contentd);
        render(renderer, "contente", contente);
        render(renderer, "msgDate", msgDate);
        render(renderer, "reloadFlag", reloadFlag);
        render(renderer, "msgStatus", msgStatus);
    }

    public void service(AuRequest request, boolean everError) {
        final String cmd = request.getCommand();
        if (cmd.equals(ON_FrontLink)) {
            Event evt = new Event(cmd, this, request.getData().get("data"));
            Events.postEvent(evt);
        }

        if (cmd.equals(ON_CentreLink)) {
            Event evt = new Event(cmd, this, request.getData().get("data"));
            Events.postEvent(evt);
        }
        if (cmd.equals(ON_TailLink)) {
            Event evt = new Event(cmd, this, request.getData().get("data"));
            Events.postEvent(evt);
        }
        if (cmd.equals(ON_Touch)) {
            Event evt = new Event(cmd, this, request.getData().get("str"));
            Events.postEvent(evt);
        }
    }

    @Override
    public void reload() {
        Widget ext = (Widget) getAttribute("$proxy");
        ext.reload(this);
    }

    public void clear() {
        Clients.evalJavaScript("zk.$('" + this.getUuid() + "').allClear()");
    }

    public void console(Object obj) {
        Clients.evalJavaScript("zk.$('" + this.getUuid() + "').console(" + obj + ")");
    }
}

