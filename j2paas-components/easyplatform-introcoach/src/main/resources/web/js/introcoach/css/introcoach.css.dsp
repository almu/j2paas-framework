<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %><%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %><%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>.z-introcoach {
	position:absolute;
	top:0;
	left:0;
	opacity:0;
	visibility:hidden
}
@keyframes expand {
	0% {
	transform:scale(0)
}
100% {
	transform:scale(1);
	opacity:1;
	visibility:visible
}
}.z-introcoach-open {
	animation:expand .8s .2s ease-in-out;
	animation-fill-mode:forwards
}
.z-introcoach-showElement,
tr.z-introcoach-showElement > td,
tr.z-introcoach-showElement > th {
    z-index: 18000 !important;
}
.z-introcoach-relativePosition,
tr.z-introcoach-showElement > td,
tr.z-introcoach-showElement > th {
    position: relative;
}
.z-introcoach-fixParent {
    z-index: auto !important;
    opacity: 1.0 !important;
    -webkit-transform: none !important;
    -moz-transform: none !important;
    -ms-transform: none !important;
    -o-transform: none !important;
    transform: none !important;
}
.z-introcoach-content {
	font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
	font-size:16px;
	font-weight:normal;
	position:relative;
	overflow:hidden;
	display:table-cell;
	min-width:180px;
	padding:12px 16px 12px 16px;
	vertical-align:middle;
	background-color:#FFFFFF;
	-webkit-border-radius:4px;
	-moz-border-radius:4px;
	-o-border-radius:4px;
	-ms-border-radius:4px;
	border-radius:4px;
	box-shadow: 0 2px 20px 0px rgb(0 0 0 / 0.2);
}
.z-introcoach-pointer {
	display:none;
	width:0;
	height:0;
	border:10px solid transparent;
	position:absolute;
	z-index:100
}
.z-introcoach-close {
	font-size:18px;
	width:18px;
	height:18px;
	position:absolute;
	top:12px;
	right:6px;
	color:rgba(0,0,0,0.57);
	cursor:pointer
}
.z-introcoach-icon {
	background-color:#FFFFFF;
	position:absolute;
	z-index:1
}
.z-introcoach-mask {
	position:fixed;
	/*background:transparent;
	animation:mask .3s .3s ease-in-out;*/
	animation-fill-mode:forwards
}
.z-introcoach-helperLayer {
    box-sizing: content-box;
    position: absolute;
    z-index: 17998;
    /*border: 1px solid rgba(0,0,0,.5);
    border-radius: 4px;*/
    box-shadow: 0 2px 20px 0px rgb(0 0 0 / 0.2);
    -webkit-transition: all 0.3s ease-out;
    -moz-transition: all 0.3s ease-out;
    -ms-transition: all 0.3s ease-out;
    -o-transition: all 0.3s ease-out;
    transition: all 0.3s ease-out;
}
.z-introcoach-layout {
    border: 1px solid red;
}
@keyframes mask {
	0% {
	background:transparent
}
100% {
	background:rgba(0,0,0,0.5)
}
}.z-introcoach-right~.z-introcoach-close {
	right:28px
}
.z-introcoach-up~.z-introcoach-close {
	top:28px
}
.z-introcoach-left {
	border-right-color:#FFFFFF
}
.z-introcoach-right {
	border-left-color:#FFFFFF
}
.z-introcoach-up {
	border-bottom-color:#FFFFFF
}
.z-introcoach-down {
	border-top-color:#FFFFFF
}
