/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.calendar.api;

import java.util.Date;

import cn.easyplatform.web.ext.calendar.Calendars;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface EventRender {
	/**
	 * Draws the day in the default mold of the calendar.
	 * @param id auto-created by the calendars component
	 */
	public String drawDay(Calendars cal, CalendarEvent evt, String id);

	/**
	 * Draws the all day in the default mold of the calendar.
	 * @param id auto-created by the calendars component
	 */
	public String drawAllDay(Calendars cal, CalendarEvent evt, String id);

	/**
	 * Draws the day in the month mold of the calendar.
	 * @param id auto-created by the calendars component
	 */
	public String drawDayByMonth(Calendars cal, CalendarEvent evt, String id);

	/**
	 * Draws the all day in the month mold of the calendar.
	 * @param id auto-created by the calendars component
	 */
	public String drawAllDayByMonth(Calendars cal, CalendarEvent evt, String id, Date begin, Date end);
}
