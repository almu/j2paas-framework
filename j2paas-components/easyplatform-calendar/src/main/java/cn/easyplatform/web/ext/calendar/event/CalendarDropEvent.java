/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.calendar.event;

import java.util.Date;
import java.util.Map;

import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.au.AuRequests;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Treeitem;

import cn.easyplatform.type.FieldVo;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.ext.calendar.Calendars;
import cn.easyplatform.web.ext.calendar.api.CalendarEvent;
import cn.easyplatform.web.ext.calendar.impl.Util;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CalendarDropEvent extends DropEvent {

	private static final long serialVersionUID = 20110830181507L;
	private Date _date;
	private CalendarEvent _ce;

	/**
	 * Converts an AU request to a drop event.
	 * 
	 * @since 5.0.0
	 */
	public static CalendarDropEvent getCalendarDropEvent(AuRequest request) {
		Calendars cmp = (Calendars) request.getComponent();
		final Map<String, Object> data = request.getData();
		final int keys = AuRequests.parseKeys(data);
		Date date = null;

		Object t = data.get("time");

		if (t != null)
			date = Util.fixDSTTime(cmp.getDefaultTimeZone(),
					new Date(Long.parseLong(t.toString())));

		return new CalendarDropEvent(request.getCommand(),
				request.getComponent(), request.getDesktop()
						.getComponentByUuid((String) data.get("dragged")),
				AuRequests.getInt(data, "x", 0),
				AuRequests.getInt(data, "y", 0), AuRequests.getInt(data,
						"pageX", 0), AuRequests.getInt(data, "pageY", 0), keys,
				date, cmp.getCalendarEventById(String.valueOf(data.get("ce"))));
	}

	public CalendarDropEvent(String name, Component target, Component dragged,
			int x, int y, int pageX, int pageY, int keys, Date date,
			CalendarEvent ce) {
		super(name, target, dragged, x, y, pageX, pageY, keys);
		_date = date;
		_ce = ce;

	}

	/**
	 * Returns the date of drop area.
	 */
	public Date getDate() {
		return _date;
	}

	@Override
	public Object getData() {
		Component c = getDragged();
		if (c instanceof Listitem) {
			Listitem li = (Listitem) c;
			if (li.getValue() instanceof ListRowVo) {
				ListRowVo rv = li.getValue();
				return rv.getData();
			} else if (li.getValue() instanceof FieldVo[]) {
				FieldVo[] fvs = li.getValue();
				Object[] data = new Object[fvs.length];
				for (int i = 0; i < data.length; i++)
					data[i] = fvs[i].getValue();
				return data;
			} else
				return li.getValue();
		} else if (c instanceof Treeitem) {
			Treeitem ti = (Treeitem) c;
			if (ti.getValue() instanceof ListRowVo) {
				ListRowVo rv = ti.getValue();
				return rv.getData();
			} else
				return ti.getValue();
		}
		return super.getData();
	}

	/**
	 * Returns the calendar event.
	 */
	public CalendarEvent getCalendarEvent() {
		return _ce;
	}
}
