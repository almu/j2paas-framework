/** Otherwise.java.

	Purpose:
		
	Description:
		
	History:
		12:48:54 PM Oct 22, 2014, Created by jumperchen

Copyright (C) 2014 Potix Corporation. All Rights Reserved.
 */
package org.zkoss.zuti.zul;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.UiException;

/**
 * A switch statement has default clause to specify a default action and
 * similar way <tt>choose</tt> has <tt>otherwise</tt> as default clause.
 * 
 * @author jumperchen
 * @since 8.0.0
 * @see Choose
 */
public class Otherwise extends TemplateBasedShadowElement {

	private static final long serialVersionUID = 2014102212485412L;

	public Otherwise() {
		_afterComposed = true; // cannot be created by afterCompose
	}

	public void beforeParentChanged(Component parent) {
		if (parent != null && !(parent instanceof Choose))
			throw new UiException("Unsupported parent for otherwise: " + parent);
		super.beforeParentChanged(parent);
	}

	protected boolean isEffective() {
		return true;
	}

	public void recreate() {
		if (isBindingReady()) { // MVVM
			Choose choose = (Choose) getParent();
			choose.compose(getShadowHost());
		} else {
			Choose choose = (Choose) getParent();
			if (choose != null) {
				choose.recreate();
			}
		}
	}

	protected void recreateDirectly() {
		super.recreate();
	}
}
