/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext;

import cn.easyplatform.type.FieldVo;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListRow {

    private Listitem item;

    /**
     * @param item
     */
    public ListRow(Listitem item) {
        this.item = item;
    }

    public Object getValue(int index) {
        FieldVo[] data = item.getValue();
        return data[index];
    }

    public Object getValue(String name) {
        FieldVo[] data = item.getValue();
        for (FieldVo fv : data) {
            if (fv.getName().equals(name))
                return fv.getValue();
        }
        return null;
    }

    public void setValue(int index, Object value) {
        FieldVo[] data = item.getValue();
        data[index].setValue(value);
        for (int i = 0; i < item.getChildren().size(); i++) {
            if (index == i) {
                ((Listcell) item.getChildren().get(i))
                        .setLabel(value == null ? "" : value.toString());
                break;
            }
        }
    }

    public void setValue(String name, Object value) {
        FieldVo[] data = item.getValue();
        int index = 0;
        for (; index < data.length; index++) {
            if (data[index].getName().equals(name))
                break;
        }
        setValue(index, value);
    }
}
