/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.builder.support;

import cn.easyplatform.web.ext.echarts.lib.graphic.*;
import cn.easyplatform.web.ext.echarts.lib.series.Graph;
import com.google.gson.*;

import java.lang.reflect.Type;

public class GraphicTypeAdapter implements JsonDeserializer<Graphic> {
    @Override
    public Graphic deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject jsonObject = (JsonObject) jsonElement;
        String tp = jsonObject.get("type").getAsString();
        switch (tp) {
            case "image":
                return jsonDeserializationContext.deserialize(jsonObject, Image.class);
            case "group":
                return jsonDeserializationContext.deserialize(jsonObject, Group.class);
            case "text":
                return jsonDeserializationContext.deserialize(jsonObject, Text.class);
            case "rect":
                return jsonDeserializationContext.deserialize(jsonObject, Rect.class);
            case "circle":
                return jsonDeserializationContext.deserialize(jsonObject, Circle.class);
            case "ring":
                return jsonDeserializationContext.deserialize(jsonObject, Ring.class);
            case "sector":
                return jsonDeserializationContext.deserialize(jsonObject, Sector.class);
            case "arc":
                return jsonDeserializationContext.deserialize(jsonObject, Arc.class);
            case "polygon":
                return jsonDeserializationContext.deserialize(jsonObject, Polygon.class);
            case "polyline":
                return jsonDeserializationContext.deserialize(jsonObject, Polyline.class);
            case "line":
                return jsonDeserializationContext.deserialize(jsonObject, Line.class);
            case "graph":
                return jsonDeserializationContext.deserialize(jsonObject, Graph.class);
            default:
                return jsonDeserializationContext.deserialize(jsonObject, BezierCurve.class);
        }
    }
}
