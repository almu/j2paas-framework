/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.util;

import org.zkoss.json.JSONAware;
import org.zkoss.json.JSONValue;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JSFunction implements JSONAware {

    private LinkedList<String> js;
    private static String FUNC = "%1$s()";
    private static String FUNC_ARG = "%1$s(%2$s)";
    private static String ARRAY_ARG = "%1$s[%2$s]";
    private static String WRAP_FUNC = "function () {this.%1$s;}";
    private static String EXEC_FUNC = "function () {%1$s;}";
    private static String RETURN_FUNC = "function () {return this.%1$s;}";
    private static String EMPTY_FUNC = "web.$void";

    public JSFunction() {
        this.js = new LinkedList();
    }

    public JSFunction evalAttribute(String attribute) {
        this.js.add(attribute);
        return this;
    }

    public JSFunction evalJavascript(String javascript) {
        this.js.add(javascript);
        return this;
    }

    public JSFunction callFunction(String funcName) {
        this.js.add(String.format(FUNC, new Object[]{funcName}));
        return this;
    }

    public JSFunction callFunction(String funcName, Object... arguments) {
        StringBuilder args = new StringBuilder();
        for (Object arg : arguments) {
            args.append(JSONValue.toJSONString(arg)).append(',');
        }
        if (arguments.length > 0) {
            args.deleteCharAt(args.length() - 1);
        }
        this.js.add(String.format(FUNC_ARG, new Object[]{funcName, args.toString()}));
        return this;
    }

    public void empty() {
        this.js.clear();
    }

    public JSFunction callArray(String attribute, int index) {
        this.js.add(String.format(ARRAY_ARG, new Object[]{attribute, Integer.valueOf(index)}));
        return this;
    }

    private boolean _reverse = false;

    public JSFunction reverse() {
        this._reverse = true;
        return this;
    }

    public String toJSONString() {
        StringBuilder sb = new StringBuilder();
        ListIterator<String> li;
        if (this._reverse) {
            li = this.js.listIterator(this.js.size());
            while (li.hasPrevious()) {
                sb.append((String) li.previous()).append('.');
            }
        } else {
            for (String s : this.js) {
                sb.append(s).append('.');
            }
        }
        if ((sb.length() > 0) && (sb.charAt(sb.length() - 1) == '.')) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public boolean isEmpty() {
        return this.js.isEmpty();
    }

    public String toWrapFunction() {
        return isEmpty() ? EMPTY_FUNC : String.format(WRAP_FUNC, new Object[]{toString()});
    }

    public String toExecFunction() {
        return isEmpty() ? EMPTY_FUNC : String.format(EXEC_FUNC, new Object[]{toString()});
    }

    public String toReturnFunction() {
        return isEmpty() ? EMPTY_FUNC : String.format(RETURN_FUNC, new Object[]{toString()});
    }

    public String toString() {
        return isEmpty() ? EMPTY_FUNC : toJSONString();
    }
}
