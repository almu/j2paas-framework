/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.builder.model;

public class EChartsDataEventImpl implements EChartsDataEvent{
    private final EChartsModel _model;
    private final int _type;
    private final Comparable _series;
    private final Comparable _category;
    private final Object _data;
    private int _sIndex = -1;
    private int _cIndex = -1;

    public EChartsDataEventImpl(EChartsModel model, int type, Comparable series, Comparable category, int seriesIndex, int categoryIndex, Object data) {
        if (model == null)
            throw new NullPointerException();
        _model = model;
        _type = type;
        _series = series;
        _data = data;
        _sIndex = seriesIndex;
        _cIndex = categoryIndex;
        _category = category;
    }

    public int getSeriesIndex() {
        return _sIndex;
    }

    public int getCategoryIndex() {
        return _cIndex;
    }

    public Comparable getCategory() {
        return _category;
    }

    public EChartsModel getModel() {
        return _model;
    }

    public int getType() {
        return _type;
    }

    public Comparable getSeries() {
        return _series;
    }

    public Object getData() {
        return _data;
    }
}
