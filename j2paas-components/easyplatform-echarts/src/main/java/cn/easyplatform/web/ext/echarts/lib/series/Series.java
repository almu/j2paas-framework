/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.Tooltip;
import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Animation;
import cn.easyplatform.web.ext.echarts.lib.support.MarkArea;
import cn.easyplatform.web.ext.echarts.lib.support.MarkLine;
import cn.easyplatform.web.ext.echarts.lib.support.MarkPoint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Series extends Animation implements Serializable {
    /**
     * 类型
     */
    protected String type;
    /**
     * 系列名称，用于tooltip的显示，legend 的图例筛选，在 setOption 更新数据和配置项时用于指定对应的系列
     */
    private String name;
    /**
     * 图形上的文本标签，可用于说明图形的一些数据信息
     */
    private LabelStyle label;
    /**
     * 折线拐点标志的样式
     */
    private ItemStyle itemStyle;
    /**
     * 图形的高亮样式。
     */
    private Emphasis emphasis;
    /**
     * 图形是否不响应和触发鼠标事件，默认为 false，即响应和触发鼠标事件
     */
    private Boolean silent;
    /**
     * 列中的数据内容数组。数组项通常为具体的数据项
     */
    private List<Object> data;
    /**
     * 图表标注
     */
    private MarkPoint markPoint;
    /**
     * 图表标线
     */
    private MarkLine markLine;
    /**
     * 图表标域，常用于标记图表中某个范围的数据，例如标出某段时间投放了广告
     */
    private MarkArea markArea;
    /**
     * 柱状图所有图形的 zlevel 值
     */
    private Integer zlevel;
    /**
     * 柱状图组件的所有图形的z值。控制图形的前后顺序。z值小的图形会被z值大的图形覆盖
     */
    private Integer z;

    private Tooltip tooltip;

    /**
     * 使用 dimensions 定义 series.data 或者 dataset.source 的每个维度的信息。
     */
    private Object dimensions;
    /**
     * 可以定义 data 的哪个维度被编码成什么
     */
    private Object encode;
    /**
     * 当使用 dataset 时，seriesLayoutBy 指定了 dataset 中用行还是列对应到系列上，也就是说，系列“排布”到 dataset 的行还是列上
     */
    private String seriesLayoutBy;
    /**
     * 如果 series.data 没有指定，并且 dataset 存在，那么就会使用 dataset。datasetIndex 指定本系列使用那个 dataset
     */
    private Integer datasetIndex;
    /**
     * 鼠标悬浮时在图形元素上时鼠标的样式是什么。同 CSS 的 cursor。
     */
    private String cursor;

    /**
     * 是否裁剪超出坐标系部分的图形，具体裁剪效果根据系列决定
     */
    private Boolean clip;

    public Boolean clip() {
        return this.clip;
    }

    public Series clip(Boolean clip) {
        this.clip = clip;
        return this;
    }

    public String cursor() {
        return this.cursor;
    }

    public Series cursor(String cursor) {
        this.cursor = cursor;
        return this;
    }

    public Integer datasetIndex() {
        return this.datasetIndex;
    }

    public Series datasetIndex(Integer datasetIndex) {
        this.datasetIndex = datasetIndex;
        return this;
    }

    public String seriesLayoutBy() {
        return this.seriesLayoutBy;
    }

    public Series seriesLayoutBy(String seriesLayoutBy) {
        this.seriesLayoutBy = seriesLayoutBy;
        return this;
    }

    public Object encode() {
        return this.encode;
    }

    public Series encode(Object encode) {
        this.encode = encode;
        return this;
    }

    public Object dimensions() {
        return this.dimensions;
    }

    public Series dimensions(Object dimensions) {
        this.dimensions = dimensions;
        return this;
    }

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return emphasis;
    }

    public Series emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Tooltip tooltip() {
        if (tooltip == null)
            tooltip = new Tooltip();
        return tooltip;
    }

    public Series tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public String type() {
        return type;
    }

    public String name() {
        return name;
    }

    public Series name(String name) {
        this.name = name;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return this.label;
    }

    public Series label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public Series itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public Boolean silent() {
        return silent;
    }

    public Series silent(Boolean silent) {
        this.silent = silent;
        return this;
    }

    public MarkPoint markPoint() {
        if (markPoint == null)
            markPoint = new MarkPoint();
        return markPoint;
    }

    public Series markPoint(MarkPoint markPoint) {
        this.markPoint = markPoint;
        return this;
    }

    public MarkLine markLine() {
        if (markLine == null)
            markLine = new MarkLine();
        return markLine;
    }

    public Series markLine(MarkLine markLine) {
        this.markLine = markLine;
        return this;
    }

    public MarkArea markArea() {
        if (markArea == null)
            markArea = new MarkArea();
        return markArea;
    }

    public Series markArea(MarkArea markArea) {
        this.markArea = markArea;
        return this;
    }

    public Series zlevel(Integer zlevel) {
        this.zlevel = zlevel;
        return this;
    }

    public Integer zlevel() {
        return this.zlevel;
    }

    public Series z(Integer z) {
        this.z = z;
        return this;
    }

    public Integer z() {
        return this.z;
    }

    public List<Object> data() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public Series data(Object values) {
        if (values instanceof List)
            this.data = (List<Object>) values;
        else {
            if (data == null)
                data = new ArrayList<Object>();
            this.data.add(values);
        }
        return this;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSilent() {
        return silent;
    }

    public void setSilent(Boolean silent) {
        this.silent = silent;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public MarkPoint getMarkPoint() {
        return markPoint;
    }

    public void setMarkPoint(MarkPoint markPoint) {
        this.markPoint = markPoint;
    }

    public MarkLine getMarkLine() {
        return markLine;
    }

    public void setMarkLine(MarkLine markLine) {
        this.markLine = markLine;
    }

    public MarkArea getMarkArea() {
        return markArea;
    }

    public void setMarkArea(MarkArea markArea) {
        this.markArea = markArea;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public Object getDimensions() {
        return dimensions;
    }

    public void setDimensions(Object dimensions) {
        this.dimensions = dimensions;
    }

    public Object getEncode() {
        return encode;
    }

    public void setEncode(Object encode) {
        this.encode = encode;
    }

    public String getSeriesLayoutBy() {
        return seriesLayoutBy;
    }

    public void setSeriesLayoutBy(String seriesLayoutBy) {
        this.seriesLayoutBy = seriesLayoutBy;
    }

    public Integer getDatasetIndex() {
        return datasetIndex;
    }

    public void setDatasetIndex(Integer datasetIndex) {
        this.datasetIndex = datasetIndex;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public Boolean getClip() {
        return clip;
    }

    public void setClip(Boolean clip) {
        this.clip = clip;
    }
}
