/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.series.base.LabelLine;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Pie extends Series {
    private Boolean legendHoverLink;
    private Boolean hoverAnimation;
    private Object selectedMode;
    private Object selectedOffset;
    private Boolean clockwise;
    private Object startAngle;
    private Object minAngle;
    private Object roseType;
    private Boolean avoidLabelOverlap;
    private Boolean stillShowZeroSum;
    private LabelLine labelLine;
    private Object center;
    private Object radius;

    /**
     * 小于这个角度（0 ~ 360）的扇区，不显示标签（label 和 labelLine）。
     */
    private Object minShowLabelAngle;

    /**
     * treemap 组件离容器左侧的距离
     */
    private Object left;
    /**
     * treemap 组件离容器上侧的距离
     */
    private Object top;
    /**
     * treemap 组件离容器右侧的距离
     */
    private Object right;
    /**
     * treemap 组件离容器下侧的距离
     */
    private Object bottom;

    private Object width;

    private Object height;

    public Pie() {
        this.type = "pie";
    }

    public Object width() {
        return this.width;
    }

    public Pie width(Object width) {
        this.width = width;
        return this;
    }

    public Object height() {
        return this.height;
    }

    public Pie height(Object height) {
        this.height = height;
        return this;
    }

    public Object left() {
        return this.left;
    }

    public Pie left(Object left) {
        this.left = left;
        return this;
    }

    public Pie left(Integer left) {
        this.left = left;
        return this;
    }

    public Object top() {
        return this.top;
    }

    public Pie top(Object top) {
        this.top = top;
        return this;
    }

    public Object right() {
        return this.right;
    }

    public Pie right(Object right) {
        this.right = right;
        return this;
    }

    public Object bottom() {
        return this.bottom;
    }

    public Pie bottom(Object bottom) {
        this.bottom = bottom;
        return this;
    }

    public Object minShowLabelAngle() {
        return minShowLabelAngle;
    }

    public Pie minShowLabelAngle(Object minShowLabelAngle) {
        this.minShowLabelAngle = minShowLabelAngle;
        return this;
    }

    public Boolean legendHoverLink() {
        return legendHoverLink;
    }

    public Pie legendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
        return this;
    }

    public Object hoverAnimation() {
        return hoverAnimation;
    }

    public Pie hoverAnimation(Boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
        return this;
    }

    public Object selectedMode() {
        return selectedMode;
    }

    public Pie selectedMode(Object selectedMode) {
        this.selectedMode = selectedMode;
        return this;
    }

    public Object selectedOffset() {
        return selectedOffset;
    }

    public Pie selectedOffset(Object selectedOffset) {
        this.selectedOffset = selectedOffset;
        return this;
    }

    public Boolean clockwise() {
        return clockwise;
    }

    public Pie clockwise(Boolean clockwise) {
        this.clockwise = clockwise;
        return this;
    }

    public Object startAngle() {
        return startAngle;
    }

    public Pie startAngle(Object startAngle) {
        this.startAngle = startAngle;
        return this;
    }

    public Object minAngle() {
        return minAngle;
    }

    public Pie minAngle(Object minAngle) {
        this.minAngle = minAngle;
        return this;
    }

    public Object roseType() {
        return roseType;
    }

    public Pie roseType(Object roseType) {
        this.roseType = roseType;
        return this;
    }

    public Boolean avoidLabelOverlap() {
        return avoidLabelOverlap;
    }

    public Pie avoidLabelOverlap(Boolean avoidLabelOverlap) {
        this.avoidLabelOverlap = avoidLabelOverlap;
        return this;
    }

    public Boolean stillShowZeroSum() {
        return stillShowZeroSum;
    }

    public Pie stillShowZeroSum(Boolean stillShowZeroSum) {
        this.stillShowZeroSum = stillShowZeroSum;
        return this;
    }

    public LabelLine labelLine() {
        if (labelLine == null)
            labelLine = new LabelLine();
        return labelLine;
    }

    public Pie labelLine(LabelLine labelLine) {
        this.labelLine = labelLine;
        return this;
    }

    public Object center() {
        return center;
    }

    public Pie center(Object value) {
       if (value instanceof String) {
            this.center = value.toString().split(",");
        } else
            this.center = value;
        return this;
    }

    public Object radius() {
        return radius;
    }

    public Pie radius(Object value) {
        if (value instanceof String) {
            this.radius = value.toString().split(",");
        } else
            this.radius = value;
        return this;
    }

    public Boolean getLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public Boolean getHoverAnimation() {
        return hoverAnimation;
    }

    public void setHoverAnimation(Boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
    }

    public Object getSelectedMode() {
        return selectedMode;
    }

    public void setSelectedMode(Object selectedMode) {
        this.selectedMode = selectedMode;
    }

    public Object getSelectedOffset() {
        return selectedOffset;
    }

    public void setSelectedOffset(Object selectedOffset) {
        this.selectedOffset = selectedOffset;
    }

    public Boolean getClockwise() {
        return clockwise;
    }

    public void setClockwise(Boolean clockwise) {
        this.clockwise = clockwise;
    }

    public Object getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(Object startAngle) {
        this.startAngle = startAngle;
    }

    public Object getMinAngle() {
        return minAngle;
    }

    public void setMinAngle(Object minAngle) {
        this.minAngle = minAngle;
    }

    public Object getRoseType() {
        return roseType;
    }

    public void setRoseType(Object roseType) {
        this.roseType = roseType;
    }

    public Boolean getAvoidLabelOverlap() {
        return avoidLabelOverlap;
    }

    public void setAvoidLabelOverlap(Boolean avoidLabelOverlap) {
        this.avoidLabelOverlap = avoidLabelOverlap;
    }

    public Boolean getStillShowZeroSum() {
        return stillShowZeroSum;
    }

    public void setStillShowZeroSum(Boolean stillShowZeroSum) {
        this.stillShowZeroSum = stillShowZeroSum;
    }

    public LabelLine getLabelLine() {
        return labelLine;
    }

    public void setLabelLine(LabelLine labelLine) {
        this.labelLine = labelLine;
    }

    public Object getCenter() {
        return center;
    }

    public void setCenter(Object center) {
        this.center = center;
    }

    public Object getRadius() {
        return radius;
    }

    public void setRadius(Object radius) {
        this.radius = radius;
    }

    public Object getMinShowLabelAngle() {
        return minShowLabelAngle;
    }

    public void setMinShowLabelAngle(Object minShowLabelAngle) {
        this.minShowLabelAngle = minShowLabelAngle;
    }

    public Object getLeft() {
        return left;
    }

    public void setLeft(Object left) {
        this.left = left;
    }

    public Object getTop() {
        return top;
    }

    public void setTop(Object top) {
        this.top = top;
    }

    public Object getRight() {
        return right;
    }

    public void setRight(Object right) {
        this.right = right;
    }

    public Object getBottom() {
        return bottom;
    }

    public void setBottom(Object bottom) {
        this.bottom = bottom;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }
}
