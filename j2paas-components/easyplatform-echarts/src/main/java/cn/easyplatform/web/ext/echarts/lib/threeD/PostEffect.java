/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import java.io.Serializable;

public class PostEffect implements Serializable {
    /**
     * 是否开启后处理特效。默认关闭。
     */
    private boolean enable;
    /**
     * 高光特效。高光特效用来表现很“亮”的颜色，
     * 因为传统的 RGB 只能表现0 - 255范围的颜色，所以对于超出这个范围特别“亮”的颜色，会通过这种高光溢出的特效去表现。
     */
    private Bloom bloom;
    /**
     * 景深效果。景深效果是模拟摄像机的光学成像效果，在对焦的区域相对清晰，离对焦的区域越远则会逐渐模糊。
     * 景深效果可以让观察者集中注意力到对焦的区域，而且让画面的镜头感更强，大景深还能塑造出微距的模型效果。
     */
    private DepthOfField depthOfField;
    /**
     * 屏幕空间的环境光遮蔽效果。环境光遮蔽效果可以让拐角处、洞、缝隙等大部分光无法到达的区域变暗，
     * 是传统的阴影贴图的补充，可以让整个场景更加自然，有层次。
     */
    private Object screenSpaceAmbientOcclusion;
    /**
     * 同 screenSpaceAmbientOcclusion
     */
    private SSAO ssao;
    /**
     * 颜色纠正和调整。类似 Photoshop 中的 Color Adjustments。
     */
    private ColorCorrection colorCorrection;
    /**
     * 在开启 postEffect 后，WebGL 默认的 MSAA (Multi Sampling Anti Aliasing) 会无法使用。
     * 这时候通过 FXAA (Fast Approximate Anti-Aliasing) 可以廉价方便的解决抗锯齿的问题，
     * FXAA 会对一些场景的边缘部分进行模糊从而解决锯齿的问题，这在一些场景上效果还不错，
     * 但是在 echarts-gl 中，需要保证很多文字和线条边缘的锐利清晰，因此 FXAA 并不是那么适用。
     * 这时候我们可以通过设置更高的devicePixelRatio来使用超采样，
     * 但是设置更高的devicePixelRatio 对电脑性能有很高的要求，
     * 所以更多时候我们建议使用 echarts-gl 中的 temporalSuperSampling，
     * 在画面静止后会持续分帧对一个像素多次抖动采样，从而达到超采样抗锯齿的效果。
     */
    private FXAA fxaa;

    public Boolean enable() {
        return this.enable;
    }
    public PostEffect enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public Bloom bloom() {
        return this.bloom;
    }
    public PostEffect bloom(Bloom bloom) {
        this.bloom = bloom;
        return this;
    }

    public DepthOfField depthOfField() {
        return this.depthOfField;
    }
    public PostEffect depthOfField(DepthOfField depthOfField) {
        this.depthOfField = depthOfField;
        return this;
    }

    public Object screenSpaceAmbientOcclusion() {
        return this.screenSpaceAmbientOcclusion;
    }
    public PostEffect screenSpaceAmbientOcclusion(Object screenSpaceAmbientOcclusion) {
        this.screenSpaceAmbientOcclusion = screenSpaceAmbientOcclusion;
        return this;
    }

    public SSAO ssao() {
        return this.ssao;
    }
    public PostEffect ssao(SSAO ssao) {
        this.ssao = ssao;
        return this;
    }

    public ColorCorrection colorCorrection() {
        return this.colorCorrection;
    }
    public PostEffect colorCorrection(ColorCorrection colorCorrection) {
        this.colorCorrection = colorCorrection;
        return this;
    }

    public FXAA fxaa() {
        return this.fxaa;
    }
    public PostEffect fxaa(FXAA fxaa) {
        this.fxaa = fxaa;
        return this;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Bloom getBloom() {
        return bloom;
    }

    public void setBloom(Bloom bloom) {
        this.bloom = bloom;
    }

    public DepthOfField getDepthOfField() {
        return depthOfField;
    }

    public void setDepthOfField(DepthOfField depthOfField) {
        this.depthOfField = depthOfField;
    }

    public Object getScreenSpaceAmbientOcclusion() {
        return screenSpaceAmbientOcclusion;
    }

    public void setScreenSpaceAmbientOcclusion(Object screenSpaceAmbientOcclusion) {
        this.screenSpaceAmbientOcclusion = screenSpaceAmbientOcclusion;
    }

    public SSAO getSsao() {
        return ssao;
    }

    public void setSsao(SSAO ssao) {
        this.ssao = ssao;
    }

    public ColorCorrection getColorCorrection() {
        return colorCorrection;
    }

    public void setColorCorrection(ColorCorrection colorCorrection) {
        this.colorCorrection = colorCorrection;
    }

    public FXAA getFxaa() {
        return fxaa;
    }

    public void setFxaa(FXAA fxaa) {
        this.fxaa = fxaa;
    }
}
