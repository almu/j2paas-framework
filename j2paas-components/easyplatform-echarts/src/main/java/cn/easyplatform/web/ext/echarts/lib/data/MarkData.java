/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;

import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MarkData implements Serializable {

    private static final long serialVersionUID = 7218201600361155091L;

    private Object type;
    private Object valueIndex;
    private Object valueDim;
    private Object[] coord;
    private Object x;
    private Object y;
    private Object value;
    private ItemStyle itemStyle;
    private ItemStyle label;
    private Object xAxis;
    private Object yAxis;

    public Object type() {
        return this.type;
    }

    public MarkData type(Object type) {
        this.type = type;
        return this;
    }

    public Object valueIndex() {
        return this.valueIndex;
    }

    public MarkData valueIndex(Object valueIndex) {
        this.valueIndex = valueIndex;
        return this;
    }

    public Object valueDim() {
        return this.valueDim;
    }

    public MarkData valueDim(Object valueDim) {
        this.valueDim = valueDim;
        return this;
    }

    public Object coord() {
        return this.coord;
    }

    public MarkData coord(Object... coord) {
        if (coord.length == 1) {
            this.coord = coord.toString().split(",");
        } else if (coord.length == 2)
            this.coord = coord;
        return this;
    }

    public Object x() {
        return this.x;
    }

    public MarkData x(Object x) {
        this.x = x;
        return this;
    }

    public Object y() {
        return this.y;
    }

    public MarkData y(Object y) {
        this.y = y;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.label;
    }

    public MarkData itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public ItemStyle label() {
        if (label == null)
            label = new ItemStyle();
        return this.label;
    }

    public MarkData label(ItemStyle label) {
        this.label = label;
        return this;
    }

    public Object xAxis() {
        return this.xAxis;
    }

    public MarkData xAxis(Object xAxis) {
        this.xAxis = xAxis;
        return this;
    }

    public Object yAxis() {
        return this.yAxis;
    }

    public MarkData yAxis(Object yAxis) {
        this.yAxis = yAxis;
        return this;
    }

    public Object value() {
        return this.value;
    }

    public MarkData value(Object value) {
        this.value = value;
        return this;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Object getValueIndex() {
        return valueIndex;
    }

    public void setValueIndex(Object valueIndex) {
        this.valueIndex = valueIndex;
    }

    public Object getValueDim() {
        return valueDim;
    }

    public void setValueDim(Object valueDim) {
        this.valueDim = valueDim;
    }

    public Object[] getCoord() {
        return coord;
    }

    public void setCoord(Object[] coord) {
        this.coord = coord;
    }

    public Object getX() {
        return x;
    }

    public void setX(Object x) {
        this.x = x;
    }

    public Object getY() {
        return y;
    }

    public void setY(Object y) {
        this.y = y;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public ItemStyle getLabel() {
        return label;
    }

    public void setLabel(ItemStyle label) {
        this.label = label;
    }

    public Object getxAxis() {
        return xAxis;
    }

    public void setxAxis(Object xAxis) {
        this.xAxis = xAxis;
    }

    public Object getyAxis() {
        return yAxis;
    }

    public void setyAxis(Object yAxis) {
        this.yAxis = yAxis;
    }
}
