/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

public class NameTextStyle extends TextStyle {
    /**
     * 标题背景颜色，默认透明
     */
    private String backgroundColor;
    /**
     * 标题边框颜色
     */
    private String borderColor;
    /**
     * borderWidth
     */
    private Integer borderWidth;
    /**
     * 圆角半径，单位px，支持传入数组分别指定 4 个圆角半径
     */
    private Object borderRadius;
    /**
     * 图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果
     */
    private Integer shadowBlur;
    /**
     * 阴影颜色
     */
    private String shadowColor;
    /**
     * 阴影水平方向上的偏移距离
     */
    private Integer shadowOffsetX;
    /**
     * 阴影垂直方向上的偏移距离
     */
    private Integer shadowOffsetY;
    /**
     * 文字垂直对齐方式，默认自动。
     */
    private String verticalAlign;
    /**
     * 文字水平对齐方式，默认自动。
     */
    private String align;
    /**
     * 文字块的内边距
     */
    private Object padding;

    public String verticalAlign() {
        return this.verticalAlign;
    }

    public NameTextStyle verticalAlign(String verticalAlign) {
        this.verticalAlign = verticalAlign;
        return this;
    }

    public String align() {
        return this.align;
    }

    public NameTextStyle align(String align) {
        this.align = align;
        return this;
    }

    public Object padding() {
        return this.padding;
    }

    public NameTextStyle padding(Object padding) {
        this.padding = padding;
        return this;
    }

    public Object borderRadius() {
        return this.borderRadius;
    }

    public NameTextStyle borderRadius(Object borderRadius) {
        this.borderRadius = borderRadius;
        return this;
    }

    public String backgroundColor() {
        return this.backgroundColor;
    }

    public NameTextStyle backgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public String borderColor() {
        return this.borderColor;
    }

    public NameTextStyle borderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Integer borderWidth() {
        return this.borderWidth;
    }

    public NameTextStyle borderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public Integer shadowBlur() {
        return this.shadowBlur;
    }

    public NameTextStyle shadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
        return this;
    }

    public String shadowColor() {
        return this.shadowColor;
    }

    public NameTextStyle shadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
        return this;
    }

    public Integer shadowOffsetX() {
        return this.shadowOffsetX;
    }

    public NameTextStyle shadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
        return this;
    }

    public Integer shadowOffsetY() {
        return this.shadowOffsetY;
    }

    public NameTextStyle shadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
        return this;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Object getBorderRadius() {
        return borderRadius;
    }

    public void setBorderRadius(Object borderRadius) {
        this.borderRadius = borderRadius;
    }

    public Integer getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    public Integer getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Integer getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }

    public String getVerticalAlign() {
        return verticalAlign;
    }

    public void setVerticalAlign(String verticalAlign) {
        this.verticalAlign = verticalAlign;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public Object getPadding() {
        return padding;
    }

    public void setPadding(Object padding) {
        this.padding = padding;
    }
}
