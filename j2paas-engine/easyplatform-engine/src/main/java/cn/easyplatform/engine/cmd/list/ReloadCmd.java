/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.list;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.dos.Record;
import cn.easyplatform.engine.runtime.ComponentProcessFactory;
import cn.easyplatform.engine.runtime.DataListProcess;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.messages.request.ListReloadRequestMessage;
import cn.easyplatform.messages.response.ListQueryResponseMessage;
import cn.easyplatform.messages.vos.datalist.ListReloadVo;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ReloadCmd extends AbstractCommand<ListReloadRequestMessage> {

    /**
     * @param req
     */
    public ReloadCmd(ListReloadRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        ListReloadVo rv = req.getBody();
        WorkflowContext ctx = cc.getWorkflowContext();
        ListContext lc = ctx.getList(rv.getId());
        if (lc == null)
            return MessageUtils.dataListNotFound(rv.getId());
        DataListProcess dlp = ComponentProcessFactory.createDatalistProcess(lc
                .getType());
        String from = lc.getHost();
        RecordContext rc = null;
        if (rv.getFromKeys() != null) {
            ListContext fc = ctx.getList(from);
            if (fc == null)
                return MessageUtils.dataListNotFound(from);
            rc = fc.getRecord(rv.getFromKeys());
            if (rc == null && fc.getType().equals(Constants.CATALOG)) {
                Record record = DataListUtils.getRecord(cc, fc,
                        rv.getFromKeys());
                rc = lc.createRecord(rv.getFromKeys(), record);
                rc.setParameter("815", false);
                rc.setParameter("814", "R");
                fc.appendRecord(rc);
            }
            if (rc == null)
                return MessageUtils.recordNotFound(fc.getBean().getTable(),
                        Lang.concat(rv.getFromKeys()).toString());
        } else
            rc = ctx.getRecord();
        return new ListQueryResponseMessage(dlp.reload(cc, lc, rc));
    }

    @Override
    public String getName() {
        return "list.Reload";
    }
}
