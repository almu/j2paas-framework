/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.task;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.ReloadRequestMessage;
import cn.easyplatform.messages.response.FieldsUpdateResponseMessage;
import cn.easyplatform.type.IResponseMessage;

import java.util.HashMap;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ReloadCmd extends AbstractCommand<ReloadRequestMessage> {

	/**
	 * @param req
	 */
	public ReloadCmd(ReloadRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		WorkflowContext ctx = cc.getWorkflowContext();
		RecordContext rc = ctx.getRecord();
		Map<String, Object> map = new HashMap<String, Object>();
		for (String name : req.getBody())
			map.put(name, rc.getValue(name));
		return new FieldsUpdateResponseMessage(map);
	}

	@Override
	public String getName() {
		return "task.Reload";
	}
}
