/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.admin;

import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.entities.beans.project.ServiceBean;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.admin.ServiceVo;
import cn.easyplatform.services.IDataSource;
import cn.easyplatform.services.IProjectService;
import cn.easyplatform.services.SystemServiceId;
import cn.easyplatform.spi.extension.ApplicationService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ServiceType;
import cn.easyplatform.type.StateType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetServicesCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public GetServicesCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        int type = (int) req.getBody();
        IProjectService ps = cc.getProjectService();
        if (type == ServiceType.JOB) {
            // 后作任务
            if (req.getActionFlag() == 0)
                return new SimpleResponseMessage(ps.getScheduleHandler().getServices());
            return new SimpleResponseMessage(ps.getCustomJob().getJobs());
        } else if (type == ServiceType.USER) {
            // 在线用户
            return new SimpleResponseMessage(ps.getSessionManager().getServices());
        } else if (type == ServiceType.DATASOURCE) {
            // 数据源
            List<ServiceVo> services = new ArrayList<>();
            if (ps != null) {
                for (ApplicationService s : ps.getServices()) {
                    if (s instanceof IDataSource) {
                        IDataSource ds = (IDataSource) s;
                        ResourceBean rb = ds.getEntity();
                        ServiceVo vo = new ServiceVo(rb.getId(), rb.getName(),
                                ServiceType.DATASOURCE, s.getState());
                        services.add(vo);
                    }
                }
            } else {
                IDataSource ds = (IDataSource) cc.getEngineConfiguration().getService(SystemServiceId.SYS_ENTITY_DS);
                ResourceBean rb = ds.getEntity();
                ServiceVo vo = new ServiceVo(rb.getId(), rb.getName(),
                        ServiceType.DATASOURCE, ds.getState());
                services.add(vo);
            }
            return new SimpleResponseMessage(services);
        } else if (type == ServiceType.CUSTOM_SERVICE) {
            List<ServiceVo> services = new ArrayList<>();
            if (ps.getEntity().getServices() != null) {
                for (ServiceBean sb : ps.getEntity().getServices()) {
                    ApplicationService as = ps.getService(sb.getClassName());
                    ServiceVo vo = new ServiceVo(sb.getClassName(), sb.getDescription(),
                            ServiceType.CUSTOM_SERVICE, as == null ? StateType.STOP : as.getState());
                    if (as != null)
                        vo.setRuntimeInfo(as.getRuntimeInfo());
                    services.add(vo);
                }
            }
            return new SimpleResponseMessage(services);
        }
        return new SimpleResponseMessage(Collections.<ServiceVo>emptyList());
    }

}
