/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.admin;

import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Nums;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.admin.HomeVo;
import cn.easyplatform.services.IProjectService;
import cn.easyplatform.services.SystemServiceId;
import cn.easyplatform.spi.service.ApiService;
import cn.easyplatform.spi.extension.ApplicationService;
import cn.easyplatform.type.IResponseMessage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.util.Map;

public class GetHomeCmd extends AbstractCommand<SimpleRequestMessage> {

    public GetHomeCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        HomeVo hv = new HomeVo();
        IProjectService ps = cc.getProjectService();
        if (ps == null) {
            int count = 0;
            for (IProjectService s : cc.getEngineConfiguration().getProjectServices()) {
                count += s.getSessionManager().getCount(null);
            }
            hv.setUsers(count);
            ApplicationService service = cc.getEngineConfiguration().getService(SystemServiceId.SYS_ENTITY_DS);
            Map<String, Object> info = (Map<String, Object>) service.getRuntimeInfo().get("basic");
            hv.setPools(new int[]{Nums.toInt(info.get("ActiveCount"), 0), Nums.toInt(info.get("MaxActive"), 0)});
            service = (ApplicationService) cc.getEngineConfiguration().getScheduleService();
            info = service.getRuntimeInfo();
            hv.setCores(new int[]{Nums.toInt(info.get("ActiveCount"), 0), Nums.toInt(info.get("MaxCount"), 0)});
        } else {
            hv.setUsers(cc.getProjectService().getSessionManager().getCount(null));
            ApplicationService service = cc.getProjectService().getService(cc.getProjectService().getEntity().getBizDb());
            Map<String, Object> info = (Map<String, Object>) service.getRuntimeInfo().get("basic");
            hv.setPools(new int[]{Nums.toInt(info.get("ActiveCount"), 0), Nums.toInt(info.get("MaxActive"), 0)});
            info = cc.getProjectService().getScheduleHandler().getRuntimeInfo();
            if (info != null)
                hv.setCores(new int[]{Nums.toInt(info.get("ActiveCount"), 0), Nums.toInt(info.get("MaxCount"), 0)});
            else
                hv.setCores(new int[]{0, 0});
        }
        return new SimpleResponseMessage(hv);
    }
}
