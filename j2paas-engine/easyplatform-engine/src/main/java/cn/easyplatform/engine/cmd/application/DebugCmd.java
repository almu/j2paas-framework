/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.application;

import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.log.ConsoleType;
import cn.easyplatform.log.LogManager;
import cn.easyplatform.messages.request.DebugRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.type.IResponseMessage;
import org.apache.logging.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DebugCmd extends AbstractCommand<DebugRequestMessage> {

    private final static Logger log = LoggerFactory.getLogger(DebugCmd.class);

    /**
     * @param req
     */
    public DebugCmd(DebugRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        if (req.getType() == DebugRequestMessage.NONE_TYPE) {
            cc.getUser().setDebugEnabled(false);
            LogManager.stopConsole(cc.getUser());
            if (log.isInfoEnabled())
                log.info("Close debugging and logger.");
        } else {
            if (req.getType() == DebugRequestMessage.DEBUG_TYPE)
                cc.getUser().setDebugEnabled(req.getBody());
            else
                cc.getUser().setDebugEnabled(false);
            if (req.getBody())
                LogManager.startConsole(ConsoleType.USER, Level.DEBUG, cc, cc.getUser().getId());
            else
                LogManager.stopConsole(cc.getUser());
            if (log.isInfoEnabled())
                log.info("Logger turn {}.", req.getBody() ? "on" : "off");
        }
        if (cc.getEngineConfiguration().getCacheManager().isCluster())
            cc.set(cc.USER_KEY, cc.getUser());
        return new SimpleResponseMessage();
    }

}
