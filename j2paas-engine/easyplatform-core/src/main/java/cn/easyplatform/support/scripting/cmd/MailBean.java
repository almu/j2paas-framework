/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.scripting.cmd;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.lang.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MailBean {

	private List<Contact> bcc;

	private List<Contact> cc;

	private List<Contact> replyTo;

	private List<Contact> to;

	private List<Attachment> attachments;

	private String subject;

	private String charset;

	private String msg;

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void addAttachment(String path, String description) {
		if (Strings.isBlank(path))
			throw new EasyPlatformWithLabelKeyException(
					"script.engine.cmd.mail.path");
		if (attachments == null)
			attachments = new ArrayList<Attachment>();
		attachments.add(new Attachment(path, description));
	}

	public void addAttachment(String path) {
		addAttachment(path, null);
	}

	public List<Contact> getBcc() {
		return bcc;
	}

	public void addBcc(String email, String name) {
		if (Strings.isBlank(email))
			throw new EasyPlatformWithLabelKeyException(
					"script.engine.cmd.mail.to");
		if (bcc == null)
			bcc = new ArrayList<Contact>();
		bcc.add(new Contact(email, name));
	}

	public void addBcc(String email) {
		addBcc(email, null);
	}

	public List<Contact> getCc() {
		return cc;
	}

	public void addCc(String email, String name) {
		if (Strings.isBlank(email))
			throw new EasyPlatformWithLabelKeyException(
					"script.engine.cmd.mail.to");
		if (cc == null)
			cc = new ArrayList<Contact>();
		cc.add(new Contact(email, name));
	}

	public void addCc(String email) {
		addCc(email, null);
	}

	public List<Contact> getReplyTo() {
		return replyTo;
	}

	public void addReplyTo(String email, String name) {
		if (Strings.isBlank(email))
			throw new EasyPlatformWithLabelKeyException(
					"script.engine.cmd.mail.to");
		if (replyTo == null)
			replyTo = new ArrayList<Contact>();
		replyTo.add(new Contact(email, name));
	}

	public void addReplyTo(String email) {
		addReplyTo(email, null);
	}

	public List<Contact> getTo() {
		return to;
	}

	public void addTo(String email, String name) {
		if (Strings.isBlank(email))
			throw new EasyPlatformWithLabelKeyException(
					"script.engine.cmd.mail.to");
		if (to == null)
			to = new ArrayList<Contact>();
		to.add(new Contact(email, name));
	}

	public void addTo(String email) {
		addTo(email, null);
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		if (Strings.isBlank(subject))
			throw new EasyPlatformWithLabelKeyException(
					"script.engine.cmd.mail.subject");
		this.subject = subject;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		if (Strings.isBlank(msg))
			throw new EasyPlatformWithLabelKeyException(
					"script.engine.cmd.mail.msg");
		this.msg = msg;
	}

	static class Attachment {
		private String path;
		private String description;

		/**
		 * @param path
		 * @param description
		 */
		public Attachment(String path, String description) {
			this.path = path;
			this.description = description;
		}

		/**
		 * @return the path
		 */
		public String getPath() {
			return path;
		}

		/**
		 * @return the description
		 */
		public String getDescription() {
			return description;
		}

	}

	static class Contact {
		private String email;

		private String name;

		/**
		 * @param email
		 * @param name
		 */
		public Contact(String email, String name) {
			this.email = email;
			this.name = name;
		}

		/**
		 * @return the email
		 */
		public String getEmail() {
			return email;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

	}
}
