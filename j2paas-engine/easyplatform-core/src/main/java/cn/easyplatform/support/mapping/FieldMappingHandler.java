/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.mapping;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.interceptor.CommandContext;

import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class FieldMappingHandler extends AbstractMappingHandler {

	private FieldDo[] source;

	/**
	 * @param cc
	 * @param target
	 */
	FieldMappingHandler(CommandContext cc, RecordContext target,
			FieldDo[] source) {
		super(cc, target);
		this.source = source;
	}

	@Override
	public List<String> doMap(String expr) {
		return null;
	}

	@Override
	protected Object getSourceValue(String name) {
		for (FieldDo fd : source) {
			if (fd.getName().equals(name))
				return fd.getValue();
		}
		return null;
	}

}
