/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.engine;

import cn.easyplatform.spi.extension.ApplicationService;
import cn.easyplatform.spi.listener.ApplicationListener;

import javax.jms.ConnectionFactory;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface Engine {

    /**
     * 初始化
     *
     * @param args
     */
    void init(Map<String, String> args);

    /**
     * 检查应用是否就绪
     */
    boolean checkModel();

    /**
     * 连接选项
     */
    void setupModel(Map<String, String> properties);

    /**
     * 启动引擎服务
     */
    void start();

    /**
     * 停止引擎
     */
    void stop();

    /**
     * 获取外部服务对象
     *
     * @param clazz
     * @return
     */
    <T> T getEngineService(Class<?> clazz);

    /**
     * 获取所有服务对象，注册RPC时使用
     *
     * @return
     */
    Set<Map.Entry<Class<?>, Object>> getEngineServices();

    /**
     * 获取外部服务对象
     *
     * @param serviceId
     * @return
     */
    ApplicationService getProjectService(String serviceId);

    /**
     * 是否保持会话存活
     *
     * @return
     */
    boolean isSessionKeepAlive();

    /**
     * 会话超时时间
     *
     * @return
     */
    int getSessionTimeout();

    /**
     * 添加应用侦听者
     *
     * @param listener
     */
    void setApplicationListener(ApplicationListener listener);

    /**
     * @return
     */
    <T> Collection<T> getProjectServices();

    /**
     * 获取系统属性
     *
     * @param name
     * @return
     */
    String getConfig(String name);
}
