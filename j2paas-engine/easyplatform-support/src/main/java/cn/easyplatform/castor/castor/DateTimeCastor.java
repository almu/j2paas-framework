/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.castor.castor;

import cn.easyplatform.castor.Castor;
import cn.easyplatform.castor.FailToCastObjectException;
import cn.easyplatform.lang.Times;

public abstract class DateTimeCastor<FROM, TO> extends Castor<FROM, TO> {

	protected java.util.Date toDate(String src) {
		try {
			return Times.D(src);
		} catch (Throwable e) {
			throw new FailToCastObjectException(e, "'%s' to %s", src,
					java.util.Date.class.getName());
		}
	}

}
