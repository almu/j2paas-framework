/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop.interceptor;

import cn.easyplatform.aop.InterceptorChain;
import cn.easyplatform.aop.MethodInterceptor;

import java.lang.reflect.Method;

/**
 * 提供一个基础实现,这个实现,既可以简化用户的实现,又可以实现与以前的Aop拦截器的兼容
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 * 
 */
public abstract class AbstractMethodInterceptor implements MethodInterceptor {

	/**
	 * 拦截方法调用, 将拦截器的行为, 分成: 之前,之后,抛异常,抛错误 -- 4种拦截点
	 */
	public void filter(InterceptorChain chain) throws Throwable {
		try {
			if (beforeInvoke(chain.getCallingObj(), chain.getCallingMethod(),
					chain.getArgs()))
				chain.doChain();
			Object obj = afterInvoke(chain.getCallingObj(), chain.getReturn(),
					chain.getCallingMethod(), chain.getArgs());
			chain.setReturnValue(obj);
		} catch (Exception e) {
			Object obj = whenException(e, chain.getCallingObj(),
					chain.getCallingMethod(), chain.getArgs());
			if (obj == null)
				throw e;
			chain.setReturnValue(obj);
		} catch (Throwable e) {
			Object obj = whenError(e, chain.getCallingObj(),
					chain.getCallingMethod(), chain.getArgs());
			if (obj == null)
				throw e;
			chain.setReturnValue(obj);
		}

	}

	/**
	 * 在方法执行前拦截
	 * 
	 * @param obj
	 *            被拦截的对象
	 * @param method
	 *            被拦截的方法
	 * @param args
	 *            被拦截的方法的参数
	 * @return 如果继续往下走,就返回true,否则就退出AOP执行链
	 */
	public abstract boolean beforeInvoke(Object obj, Method method,
			Object... args);

	/**
	 * 在方法执行后拦截
	 * 
	 * @param obj
	 *            被拦截的对象
	 * @param returnObj
	 *            被拦截的方法的返回值的对象
	 * @param method
	 *            被拦截的方法
	 * @param args
	 *            被拦截方法的参数
	 * @return 将会替代原方法返回值的值
	 */
	public abstract Object afterInvoke(Object obj, Object returnObj,
			Method method, Object... args);

	/**
	 * 抛出Exception的时候拦截
	 * 
	 * @param e
	 *            异常对象
	 * @param obj
	 *            被拦截的对象
	 * @param method
	 *            被拦截的方法
	 * @param args
	 *            被拦截方法的返回值
	 * @return 返回处理的对象
	 */
	public abstract Object whenException(Exception e, Object obj,
			Method method, Object... args);

	/**
	 * 抛出Throwable的时候拦截
	 * 
	 * @param e
	 *            异常对象
	 * @param obj
	 *            被拦截的对象
	 * @param method
	 *            被拦截的方法
	 * @param args
	 *            被拦截方法的返回值
	 * @return 返回处理的对象
	 */
	public abstract Object whenError(Throwable e, Object obj, Method method,
			Object... args);

}
