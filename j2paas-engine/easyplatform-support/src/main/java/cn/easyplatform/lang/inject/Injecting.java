/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.inject;

/**
 * 抽象注入接口
 * <p>
 * 封装了通过 setter 以及 field 两种方式设置值的区别
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 */
public interface Injecting {

	/**
	 * 通过反射，向对象某一字段设置一个值
	 * 
	 * @param obj
	 *            被设值的对象
	 * @param value
	 *            值
	 */
	void inject(Object obj, Object value);

}