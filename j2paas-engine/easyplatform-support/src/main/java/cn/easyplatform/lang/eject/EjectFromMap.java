/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.eject;

import java.util.Map;

public class EjectFromMap implements Ejecting {

	private String key;

	public EjectFromMap(String key) {
		this.key = key;
	}

	public Object eject(Object obj) {
		return null == obj ? null : ((Map<?, ?>) obj).get(key);
	}

}
