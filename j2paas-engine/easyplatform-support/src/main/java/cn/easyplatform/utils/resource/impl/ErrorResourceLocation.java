/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.utils.resource.impl;

import cn.easyplatform.utils.resource.GResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.regex.Pattern;

public class ErrorResourceLocation extends ResourceLocation {
	public void scan(String base, Pattern pattern, List<GResource> list) {
	}

	private static final Logger log = LoggerFactory.getLogger(ErrorResourceLocation.class);

	Object loc;

	public ErrorResourceLocation(Object loc) {
		this.loc = loc;
		if (log.isInfoEnabled())
			log.info("ErrorResourceLocation [loc=" + loc
					+ "], maybe it is in your classpath, but not exist");
	}

	public String toString() {
		return "ErrorResourceLocation [loc=" + loc + "]";
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((loc == null) ? 0 : loc.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErrorResourceLocation other = (ErrorResourceLocation) obj;
		if (loc == null) {
			if (other.loc != null)
				return false;
		} else if (!loc.equals(other.loc))
			return false;
		return true;
	}
}