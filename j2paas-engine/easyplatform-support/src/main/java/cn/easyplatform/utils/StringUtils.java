/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.utils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StringUtils {

    public static boolean hasLength(CharSequence str) {
        return (str != null && str.length() > 0);
    }

    public static boolean hasText(CharSequence str) {
        if (!hasLength(str)) {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    /**
     * 格式化指定的对象
     *
     * @param fmt
     * @param v
     * @return
     */
    public static String format(String fmt, Object v) {
        if (v instanceof Date) {
            Date date = (Date) v;
            if (fmt.equals("medium+short")) {
                return DateFormat.getDateInstance(DateFormat.LONG).format(date) + " " + new SimpleDateFormat("HH:mm:ss").format(date);
            } else if (fmt.equals("medium")) {
                return DateFormat.getDateInstance(DateFormat.MEDIUM).format(date);
            } else if (fmt.equals("long")) {
                return new SimpleDateFormat("HH:mm:ss").format(date);
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat(fmt);
                return sdf.format(date);
            }
        } else if (v instanceof Number) {
            DecimalFormat df = new DecimalFormat(fmt);
            return df.format(v);
        }
        return v.toString();
    }
}
