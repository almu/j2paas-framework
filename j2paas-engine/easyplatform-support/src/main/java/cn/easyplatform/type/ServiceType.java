/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.type;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ServiceType {

    /**
     * 项目服务
     */
    public final static int PROJECT = 1;

    /**
     * 资源监控
     */
    public final static int RESOURCE = 5;

    /**
     * 数据源
     */
    public final static int DATASOURCE = 2;

    /**
     * 计划任务
     */
    public final static int JOB = 3;

    /**
     * 用户
     */
    public final static int USER = 4;

    public final static int API = 5;

    public final static int NOTICE = 6;

    public final static int ROLE = 7;

    public final static int MENU = 8;

    public final static int SYSLOG = 9;

    public final static int APPLOG = 10;

    public final static int USERLOG = 11;

    public final static int CUSTOM_SERVICE = 12;

}
