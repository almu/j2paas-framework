/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.project;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = { "onInitQuery", "devices" })
@XmlAccessorType(XmlAccessType.NONE)
public class PortletBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5904087211626589604L;

	@XmlAttribute
	private String name;

	@XmlAttribute
	private String desp;

	@XmlElement
	private String onInitQuery;

	@XmlElement(name = "device")
	private List<DeviceMapBean> devices;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the desp
	 */
	public String getDesp() {
		return desp;
	}

	/**
	 * @param desp
	 *            the desp to set
	 */
	public void setDesp(String desp) {
		this.desp = desp;
	}

	/**
	 * @return the devices
	 */
	public List<DeviceMapBean> getDevices() {
		return devices;
	}

	/**
	 * @param devices
	 *            the devices to set
	 */
	public void setDevices(List<DeviceMapBean> devices) {
		this.devices = devices;
	}

	/**
	 * @return the onInitQuery
	 */
	public String getOnInitQuery() {
		return onInitQuery;
	}

	/**
	 * @param onInitQuery the onInitQuery to set
	 */
	public void setOnInitQuery(String onInitQuery) {
		this.onInitQuery = onInitQuery;
	}
}
