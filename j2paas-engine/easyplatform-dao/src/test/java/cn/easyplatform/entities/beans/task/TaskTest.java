/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.task;

import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.lang.Dumps;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TaskTest {
	@Test
	public void testJava2Xml() {
		TaskBean tb = new TaskBean();
		tb.setRefId("WZ003");
		tb.setProcessCode("U");
		tb.setUpdatable(true);
		tb.setVisible(true);
		EventLogic el = new EventLogic();
		el.setContent("${azd} = 200;");
		tb.setOnInit(el);
		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/task");
			File file = new File(url.getFile() + "/task1.xml");
			TransformerFactory.newInstance().transformToXml(tb,
					new FileOutputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testXml2Java() {
		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/task/task1.xml");
			TaskBean lb = TransformerFactory.newInstance().transformFromXml(
					TaskBean.class, url.openStream());
			System.out.println(Dumps.obj(lb));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
